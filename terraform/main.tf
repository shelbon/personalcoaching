terraform { 
  required_providers {
    scaleway = {
      source  = "scaleway/scaleway"
      version = "2.29.0"
    }
    kubectl = {
      source  = "alekc/kubectl"
      version = "2.0.3"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.11.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.23.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.4.0"
    }
  }

  backend "s3" {
    bucket                      = "esgi-pyd-bucket"
    key                         = "cluster.tfstate"
    region                      = "fr-par"
    skip_credentials_validation = true
    skip_region_validation      = true
    skip_requesting_account_id  = true
    endpoints = {
      s3 = "https://s3.fr-par.scw.cloud"
    }
  }
}
locals {
  kubernetes_config = {
    host = scaleway_k8s_cluster.this.apiserver_url
    cluster_ca_certificate = base64decode(
      scaleway_k8s_cluster.this.kubeconfig[0].cluster_ca_certificate
    )
    token = scaleway_k8s_cluster.this.kubeconfig[0].token
  }
  kubernetes_test_config = {
    config_path      = "/home/patrick/.kube/config/kubeconfig.yml"
    config_context   = "minikube"
    load_config_file = false
  }
}
provider "kubectl" {
  host                   = local.kubernetes_config.host
  cluster_ca_certificate = local.kubernetes_config.cluster_ca_certificate
  token                  = local.kubernetes_config.token
  load_config_file       = false
}
provider "helm" {
  kubernetes {
    host                   = local.kubernetes_config.host
    cluster_ca_certificate = local.kubernetes_config.cluster_ca_certificate
    token                  = local.kubernetes_config.token
  }
}
provider "scaleway" {}
 provider "kubernetes" {
   host                   = local.kubernetes_config.host
   cluster_ca_certificate = local.kubernetes_config.cluster_ca_certificate
   token                  = local.kubernetes_config.token
 }
 resource "local_file" "kubeconfig" {
   filename        = "${path.module}/kubeconfig"
   file_permission = "0600"
   content         = scaleway_k8s_cluster.this.kubeconfig[0].config_file
 }
resource "scaleway_vpc_private_network" "this" {
  name = "cluster-groupe4-5al1-network"
}
 resource "scaleway_k8s_cluster" "this" {
   name                        = "cluster2-groupe4-5al1"
   version                     = "1.28.2"
   cni                         = "cilium"
   private_network_id          = scaleway_vpc_private_network.this.id
   delete_additional_resources = true
 }
 resource "helm_release" "nginx-controller" {
   name       = "nginx-controller"
   chart      = "ingress-nginx"
   repository = "https://kubernetes.github.io/ingress-nginx"
   namespace  = kubernetes_namespace.this.metadata[0].name
 }
 resource "scaleway_k8s_pool" "this" {
   cluster_id = scaleway_k8s_cluster.this.id
   name       = "pool-groupe4-5al1"
   node_type  = "DEV1-M"
   size       = 1
 }
variable "db_name" {
  type      = string
  sensitive = true
  default   = "coach_me"
}
variable "db_password" {
  type      = string
  sensitive = true
}
variable "db_username" {
  type      = string
  sensitive = true

}
variable "SERVER_PORT" {
  type      = string
  sensitive = true
  default   = "8080"
}
variable "app_version" {
  type = string
}
variable "allowed_origins" {
 description = "The allowed origins for the deployment resource"
 type      = string
 default   = null
 sensitive = true
 nullable  = true

}

 resource "kubernetes_namespace" "this" {
   depends_on = [scaleway_k8s_pool.this, scaleway_k8s_cluster.this]
   metadata {
     name = "groupe4-5al1"
   }
 }
 module "scaleway_database" {
   source      = "./modules/scaleway_database"
   db_username = var.db_username
   db_name     = var.db_name
   db_password = var.db_password
 }
 module "kubernetes" {
   source      = "./modules/kubernetes"
   db_username = var.db_username
   db_name     = var.db_name
   db_password = var.db_password
   db_url      = module.scaleway_database.coachme_db_ip
   db_port     = module.scaleway_database.coachme_db_port
   app_version = var.app_version
   server_port = var.SERVER_PORT
   allowed_origins=var.allowed_origins
   namespace   = kubernetes_namespace.this.metadata[0].name
 }
