terraform {
  required_providers {
    scaleway = {
      source  = "scaleway/scaleway"
      version = "2.29.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.4.0"
    }
  }
  backend "local" {
    path = "terraform.tfstate"
  }
}
resource "scaleway_object_bucket" "this" {
  name = "esgi-pyd-bucket"
}
output "endpoint" {
  value = "scaleway_object_bucket.this.endpoint"
}
