variable "SERVER_PORT" {
  type      = string
  sensitive = true
  default   = "8080"
}
variable "kubernetes_namespace" {
  type = string
}
resource "kubernetes_service_v1" "clusterip" {
  metadata {
    namespace = var.kubernetes_namespace
    labels = {
      app = "coachme-app"
    }
    name = "clusterip-coachme"
  }
  spec {
    type = "ClusterIP"
    port {
      name        = "8099-dynamic"
      port        = 8099
      protocol    = "TCP"
      target_port = var.SERVER_PORT
    }
    selector = {
      app = "coachme-app"
    }
  }
}
