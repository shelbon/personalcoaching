variable "namespace" {
  type = string
}
module "cert_manager" {
  namespace_name                         = var.namespace
  source                                 = "terraform-iaac/cert-manager/kubernetes"
  version                                = "2.6.2"
  cluster_issuer_email                   = "mouclepatrick@pm.me"
  cluster_issuer_private_key_secret_name = "letsencrypt-staging"
  create_namespace                       = false
}

resource "kubernetes_ingress_v1" "ingress" {
  metadata {
    name      = "ingress-controller"
    namespace = var.namespace
    annotations = {
      "kubernetes.io/ingress.class"                = "nginx"
      "nginx.ingress.kubernetes.io/rewrite-target" = "/"
      "cert-manager.io/cluster-issuer"             = module.cert_manager.cluster_issuer_name
    }

  }
  wait_for_load_balancer=true
  spec {
    rule {
      http {
        path {
          path_type = "ImplementationSpecific"
          backend {
            service {
              name = "clusterip-coachme"
              port {
                number = 8099
              }
            }
          }

          path = "/"
        }
      }
    }
  }
}
output "load_balancer_ip" {
  value = kubernetes_ingress_v1.ingress.status.0.load_balancer.0.ingress.0.ip
}
