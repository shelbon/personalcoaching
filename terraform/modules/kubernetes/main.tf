variable "db_name" {
  type      = string
  sensitive = true
  default   = "coach_me"
}
variable "db_password" {
  type      = string
  sensitive = true
}
variable "db_username" {
  type      = string
  sensitive = true

}

variable "db_port" {
  type      = string
  sensitive = true

}
variable "db_url" {
  type      = string
  sensitive = true
}
variable "app_version" {
  type = string
}
variable "server_port" {
  default = 8080
  type    = string
}
variable "allowed_origins" {
  type      = string 
  sensitive = true
}
variable "namespace" {
  type = string
}
module "ingress" {
  source    = "./ingress"
  namespace = var.namespace
}
module "cluster_ip" {
  source               = "./cluster_ip"
  kubernetes_namespace = var.namespace
}
resource "kubernetes_deployment_v1" "app" {
  depends_on = [module.ingress, module.cluster_ip]
  metadata {
    namespace = var.namespace
    name      = "coachme-app"
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "coachme-app"
      }
    }
    template {
      metadata {
        labels = {
          app = "coachme-app"
        }
      }
      spec {
        container {
          image = "docker.io/shelbon/coachme-app:${var.app_version}"
          name  = "coachme-app"
          env {
            name  = "DB_NAME"
            value = var.db_name
          }
          env {
            name  = "DB_PASSWORD"
            value = var.db_password
          }
          env {
            name  = "DB_URL"
            value = var.db_url
          }
          env {
            name  = "DB_USERNAME"
            value = var.db_username
          }
          env {
            name  = "DB_PORT"
            value = var.db_port
          }
          env {
            name  = "SERVER_PORT"
            value = var.server_port
          }
          env {
            name  = "ALLOWED_ORIGINS"
            value = var.allowed_origins ==null ? "https://${module.ingress.load_balancer_ip}":""
          }
          port {
            container_port = var.server_port
            name           = "app-port"
          }
        }
      }
    }
  }
}
