terraform {
  required_providers {
    scaleway = {
      source  = "scaleway/scaleway"
      version = "2.29.0"
    }
  }
}
variable "db_name" {
  type      = string
  sensitive = true
  default   = "coach_me"
}
variable "db_password" {
  type      = string
  sensitive = true
}
variable "db_username" {
  type      = string
  sensitive = true

}

resource "scaleway_rdb_instance" "postgres_instance" {
  name           = "postgresSQL"
  node_type      = "DB-DEV-S"
  engine         = "PostgreSQL-15"
  is_ha_cluster  = false
  disable_backup = true
  user_name      = var.db_username
  password       = var.db_password
}
resource "scaleway_rdb_database" "coachme_db" {
  instance_id = scaleway_rdb_instance.postgres_instance.id
  name        = var.db_name
}

resource "scaleway_rdb_privilege" "main" {
  instance_id   = scaleway_rdb_instance.postgres_instance.id
  user_name     = var.db_username
  database_name = var.db_name
  permission    = "all"
  depends_on    = [scaleway_rdb_database.coachme_db]
}
output "coachme_db_ip" {
  value = scaleway_rdb_instance.postgres_instance.load_balancer[0].ip
}
output "coachme_db_port" {
  value = scaleway_rdb_instance.postgres_instance.endpoint_port
}
