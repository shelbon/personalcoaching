package com.mouclepatrick.personalcoaching;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.test.context.ActiveProfiles;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

import static com.mouclepatrick.personalcoaching.utils.CSRF.CSRF_COOKIE;
import static com.mouclepatrick.personalcoaching.utils.CSRF.REQUEST_HEADER_NAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.HttpMethod.POST;

@SpringBootTest(
        classes = IntegrationTestsApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@ActiveProfiles(value="test")
public class SecurityConfigurationTest {

    private static final String USERNAME = "johndoe-coachme@trash-mail.com";
    private static final String PASSWORD = "John123**";
    private static final String CSRF_TOKEN = UUID.randomUUID().toString();
    private static final String DIFFERENT_CSRF_TOKEN = UUID.randomUUID().toString();

    @Resource
    private TestRestTemplate rest;


    @Test
    public void blockAuthenticationWhenCSRFTokensAreAbsent() throws IOException {

        ClientHttpResponse clientHttpResponse = login(USERNAME, PASSWORD);

        assertThat(clientHttpResponse).isNotNull();
        assertThat(clientHttpResponse.getStatusCode()).isNotNull();
    }

    @Test
    public void blockAuthenticationWhenCSRFTokenIsAbsentFromHeader() throws IOException {

        ClientHttpResponse clientHttpResponse = login(USERNAME, PASSWORD, null, CSRF_TOKEN);

        assertThat(clientHttpResponse).isNotNull();
        assertEquals(HttpStatus.FORBIDDEN, clientHttpResponse.getStatusCode());
    }

    @Test
    public void blockAuthenticationWhenCSRFTokenIsAbsentFromCookie() throws IOException {

        ClientHttpResponse clientHttpResponse = login(USERNAME, PASSWORD, CSRF_TOKEN, null);

        assertThat(clientHttpResponse).isNotNull();
        assertEquals(HttpStatus.FORBIDDEN, clientHttpResponse.getStatusCode());
    }

    @Test
    public void blockAuthenticationWhenCSRFTokensDoNotMatch() throws IOException {

        ClientHttpResponse clientHttpResponse = login(USERNAME, PASSWORD, CSRF_TOKEN, DIFFERENT_CSRF_TOKEN);

        assertThat(clientHttpResponse).isNotNull();
        assertEquals(HttpStatus.FORBIDDEN, clientHttpResponse.getStatusCode());
    }

    private ClientHttpResponse login(String username, String password) {
        return rest.execute(
                "/api/auth/login",
                POST,
                request -> {
                    OutputStream body = request.getBody();
                    body.write(("username=" + username + "&password=" + password).getBytes());
                    body.flush();
                    body.close();
                },
                response -> response
        );
    }

    private ClientHttpResponse login(String username, String password, String csrfToken) {
        return login(username, password, csrfToken, csrfToken);
    }

    private ClientHttpResponse login(String username, String password, String csrfHeaderToken, String csrfCookieToken) {
        return rest.execute(
                "/api/auth/login",
                POST,
                request -> {
                    // Body
                    OutputStream body = request.getBody();
                    body.write(("username=" + username + "&password=" + password).getBytes());
                    body.flush();
                    body.close();

                    // Headers
                    HttpHeaders headers = request.getHeaders();
                    if (csrfHeaderToken != null) {
                        headers.set(HttpHeaders.COOKIE, CSRF_COOKIE + "=" + csrfHeaderToken);
                    }
                    if (csrfCookieToken != null) {
                        headers.set(REQUEST_HEADER_NAME, csrfCookieToken);
                    }
                },
                response -> response
        );
    }
}
