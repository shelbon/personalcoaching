package com.mouclepatrick.personalcoaching.services;

import com.mouclepatrick.personalcoaching.models.Activite;
import com.mouclepatrick.personalcoaching.models.Programme;
import com.mouclepatrick.personalcoaching.models.Reservation;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;

import java.time.LocalDate;

import static com.googlecode.catchexception.apis.BDDCatchException.caughtException;
import static com.googlecode.catchexception.apis.BDDCatchException.when;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.BDDAssertions.then;


@SpringBootTest
@ActiveProfiles("test")
class ProgrammeServiceTest {
    @Autowired
    ProgrammeService programmeService;
    @Autowired
    CoachService coachService;

    @Test
    void getProgramme() {
        Programme existingProgramme = programmeService.getProgramme(1);
        assertThat(existingProgramme).isNotNull();
        assertThat(existingProgramme.getNom()).isEqualTo("Les bases du sql");

    }

    @Test
    @Transactional
    void fetchingNonexistentProgrameThrowException() {
        when(() -> programmeService.deleteProgramme(0));
        then(caughtException()).isInstanceOf(EmptyResultDataAccessException.class);
    }

    @Test
    @Transactional
    void cancelingProgrammeCancelReservation() {
        programmeService.cancelProgramme(1);
        Programme programme = programmeService.getProgramme(1);
        for (Activite activite : programme.getActivites()) {
            activite.getReservations().forEach((reservation) -> {
                if (reservation.getEtatSeance().
                        contains(Reservation.Etat.CANCELED.toString())
                        ||
                        reservation.getEtatSeance()
                                .contains(Reservation.Etat.PENDING.toString())
                        ||
                        reservation.getEtatSeance()
                                .contains(Reservation.Etat.ACCEPTED.toString())) {
                    assertThat(reservation.getDateAnnulation()).isNotNull();
                } else {
                    assertThat(reservation.getDateAnnulation()).isNull();
                }

            });
        }
        assertThat(programme.getDateAnnulation()).isEqualTo(LocalDate.now().toString());

    }

    @Test
    @Transactional
    void cancelingMissingProgrammeThrowsException() {
        when(() -> programmeService.cancelProgramme(0));
        then(caughtException()).isInstanceOf(NullPointerException.class);
    }

    @Test
    @Transactional
    void updateingProgrameReturnIt() {
        Programme existingProgramme = programmeService.getProgramme(1);
        existingProgramme.setDateFin(LocalDate.parse("2022-02-26"));
        var modifiedProgramme = programmeService.updateProgramme(existingProgramme);
        assertThat(modifiedProgramme.getDateFin()).isEqualTo("2022-02-26");
    }

    @Test
    @Transactional
    void creatingaProgrammeReturnIt() {
        var programme = new Programme();
        programme.setNom("Preview,What's a distributed sql database");
        programme.setDateDebut(LocalDate.parse("2022-10-28"));
        programme.setDateDebut(LocalDate.parse("2022-11-28"));
        programme.setDateAnnulation(null);
        programme.setSujets(null);
        programme.setActivites(null);
        Programme newProgramme = programmeService.createProgramme(programme);
        assertThat(newProgramme).isNotNull();
        assertThat(newProgramme).isEqualTo(programme);

    }

    @Test
    @Transactional
    void deleteProgramme() {
        programmeService.deleteProgramme(1);
        var programmeDeleted = programmeService.getProgramme(1);
        assertThat(programmeDeleted).isNull();
    }
    @Test
    void isCoachProgrammeShouldReturnTrue() {
        boolean isCoachProgramme=programmeService.isCoachProgramme(2,1);
        assertThat(isCoachProgramme).isTrue();
    }
    @Test
    void isCoachProgrammeShouldReturnFalse(){
        boolean isCoachProgramme=programmeService.isCoachProgramme(2,6);
        assertThat(isCoachProgramme).isFalse();
    }

}
