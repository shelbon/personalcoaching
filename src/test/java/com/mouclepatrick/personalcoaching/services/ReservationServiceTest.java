package com.mouclepatrick.personalcoaching.services;

import com.mouclepatrick.personalcoaching.models.Activite;
import com.mouclepatrick.personalcoaching.models.Client;
import com.mouclepatrick.personalcoaching.models.Coach;
import com.mouclepatrick.personalcoaching.models.Reservation;
import com.mouclepatrick.personalcoaching.repositories.ActiviteRepository;
import com.mouclepatrick.personalcoaching.repositories.UtilisateurRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles(value="test")
class ReservationServiceTest {
    @Autowired
    private ReservationService reservationService;
    @Autowired
    private ActiviteRepository activiteRepository;
    @Autowired
    UtilisateurRepository utilisateurRepository;

    @Test
    @Transactional
    void createReservation() {
        Reservation reservation = new Reservation();
        Activite activite = activiteRepository.findById(1).orElse(null);
        Coach coach = activite.getProgramme().getCoach();
        Client client = (Client) utilisateurRepository.findById(1).orElse(null);
        LocalDateTime dateSeance = LocalDateTime.parse("2022-02-25T15:00:00");
        reservation.setClient(client);
        reservation.setCoach(coach);
        reservation.setHeureSeance(dateSeance.toLocalTime());
        reservation.setDateReservation(LocalDateTime.now());
        reservation.setDateSeance(dateSeance.toLocalDate());
        reservation.setDescription("Test");
        reservation.setActivite(activite);
        coach.addReservation(reservation);
        Reservation savedReservation = reservationService.add(reservation);
        assertThat(savedReservation.getActivite().getDescription()).isEqualTo(reservation.getActivite().getDescription());
    }
}
