package com.mouclepatrick.personalcoaching.services;

import com.mouclepatrick.personalcoaching.models.Client;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles(value="test")
public class ClientServiceTest {
    @Autowired
    private ClientService clientService;
    @Autowired
    private CoachService coachService;

    @Autowired
    private ProgrammeService programmeService;

    @Test
    @Transactional
    void creerClient() {
        Client client = new Client();
        client.setNom("bobby");
        client.setPrenom("bob");
        var clientCree = clientService.add(client);
        assertThat(clientCree).isEqualTo(client);
    }

    @Test
    @Transactional
    void modifierClient() {
        var client = clientService.findBy(1);
        clientService.findAll();
        client.setNom("bobby");
        var clientModifie = clientService.add(client);
        assertThat(clientModifie.getNom()).isEqualTo(client.getNom());
    }

}
