package com.mouclepatrick.personalcoaching.services;

import com.mouclepatrick.personalcoaching.controllers.exception.ResourceNotFoundException;
import com.mouclepatrick.personalcoaching.dto.CoachProfileDTO;
import com.mouclepatrick.personalcoaching.models.Coach;
import com.mouclepatrick.personalcoaching.models.Reservation;
import com.mouclepatrick.personalcoaching.repositories.ReservationRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import static com.googlecode.catchexception.apis.BDDCatchException.caughtException;
import static com.googlecode.catchexception.apis.BDDCatchException.when;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.then;


@SpringBootTest
@ActiveProfiles(value="test")
class CoachServiceTest {
    @Autowired
    private CoachService coachService;
    @Autowired
    private ReservationRepository reservationRepository;
    @Test
    void getExistingCoachReturnCoach() {
        Coach coach = coachService.getCoach(2);
        assertThat(coach.getNom()).isEqualTo("doe");
    }

    @Test
    void updateNonExistingCoachThrowsException() {
        Coach coach = new Coach();
        coach.setId(1000000);
        when(() -> coachService.updateCoach(coach));
        then(caughtException()).isInstanceOf(ResourceNotFoundException.class);
    }

    @Test
    @Transactional
    void whenCreatingACoachReturnIt() {
        Coach newCoach = new Coach();
        newCoach.setNom("Singer");
        newCoach.setPrenom("Bob");
        newCoach.setEmail("email");
        newCoach.setTel("0000000");
        newCoach.setPassword("password");
        Coach addedCoach = coachService.addCoach(newCoach);
        assertThat(addedCoach.getNom()).isEqualTo("Singer");
        assertThat(addedCoach.getPrenom()).isEqualTo("Bob");

    }

    @Test
    @Transactional
    void whenUpdatingCoachReturnModifiedCoach() {
        Coach coach = coachService.getCoach(2);
        coach.setNom("bob");
        coachService.updateCoach(coach);
        assertThat(coach.getNom()).isEqualTo("bob");
    }

    @Test
    @Transactional
    void deleteCoach() {
        coachService.deleteCoach(2);
        var coachDeleted = coachService.getCoach(1);
        assertThat(coachDeleted).isNull();
    }

    @Test
    void convert() {
        CoachProfileDTO coachProfileDTO = CoachProfileDTO.builder()
                .id(2)
                .nom("imposter")
                .heureDebut("09:00:00")
                .heureFin("17:00:00")
                .build();
        var coach = coachService.convertToEntity(coachProfileDTO);
        Coach expectedResult = coachService.getCoach(2);
        expectedResult.setNom("imposter");
        assertThat(coach.getNom()).isEqualTo(expectedResult.getNom());
    }


    @Test
    void getTodayPendingSeances() {
        var coachID=2;
        var todayDate= LocalDate.now();
        var seance =new Reservation();
        var coach=coachService.getCoach(coachID);
        seance.setDateSeance(todayDate);
        seance.setHeureSeance(LocalTime.now());
        seance.setCoach(coach);
        seance.setEtatSeance(Reservation.Etat.PENDING.toString());
         Reservation newSeance= reservationRepository.save(seance);
        List<Reservation> seances = coachService.getTodayPendingSeances(coachID);
        assertThat(seances).anyMatch(thisSeance-> thisSeance.getId().equals(newSeance.getId()));
    }

    @Test
    void getUpcomingSeances() {
        var coachID=2;
        var todayDate= LocalDate.now();
        var seance =new Reservation();
        var coach=coachService.getCoach(coachID);
        seance.setDateSeance(todayDate);
        seance.setHeureSeance(LocalTime.now().plusMinutes(10));
        seance.setCoach(coach);
        seance.setEtatSeance(Reservation.Etat.ACCEPTED.toString());
        Reservation newSeance= reservationRepository.save(seance);
        List<Reservation> seances = coachService.getUpcomingSeances(coachID);
        assertThat(seances).anyMatch(thisSeance-> thisSeance.getId().equals(newSeance.getId()));
    }
    @Test
    void getUpcomingSeancesWHenSeanceISMoreThanFiveHours() {
        var coachID=2;
        var todayDate= LocalDate.now();
        var seance =new Reservation();
        var coach=coachService.getCoach(coachID);
        seance.setDateSeance(todayDate);
        seance.setHeureSeance(LocalTime.now().plusHours(6));
        seance.setCoach(coach);
        seance.setEtatSeance(Reservation.Etat.ACCEPTED.toString());
        reservationRepository.save(seance);
        Reservation newSeance= reservationRepository.save(seance);
        List<Reservation> seances = coachService.getUpcomingSeances(coachID);

        assertThat(seances).noneMatch(thisSeance-> thisSeance.getId().equals(newSeance.getId()));
    }
    @Test
    void getUpcomingSeancesWHenSeanceIsBehindCurrentHour() {
        var coachID=2;
        var todayDate= LocalDate.now();
        var seance =new Reservation();
        var coach=coachService.getCoach(coachID);
        seance.setDateSeance(todayDate);
        seance.setHeureSeance(LocalTime.now().minusHours(6));
        seance.setCoach(coach);
        seance.setEtatSeance(Reservation.Etat.ACCEPTED.toString());
        reservationRepository.save(seance);
        Reservation newSeance= reservationRepository.save(seance);
        List<Reservation> seances = coachService.getUpcomingSeances(coachID);

        assertThat(seances).noneMatch(thisSeance-> thisSeance.getId().equals(newSeance.getId()));
    }

    @Test
    void accepterSeance() {
        int seanceId=3;
        String url="https://www.example.com";
        Reservation seance= reservationRepository.findById(seanceId).orElse(null);
        seance.setUrl(url);
        Reservation accepetedSeance= coachService.accepterSeance(seance);
        assertThat(accepetedSeance.getEtatSeance()).isEqualTo(Reservation.Etat.ACCEPTED.toString());
        assertThat(accepetedSeance.getUrl()).isEqualTo(url);
    }
}
