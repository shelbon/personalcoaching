package com.mouclepatrick.personalcoaching.response;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
@Setter
public class ApiResponseBody<T> extends ApiResponse {
    private T payload;

    public ApiResponseBody(HttpStatus status, LocalDateTime timestamp, String message) {
        super(status, timestamp, message);
    }

    public ApiResponseBody(HttpStatus status, String message, T payload) {
        super(status, message);
        this.payload = payload;
    }

    public ApiResponseBody(HttpStatus status, T payload) {
        super(status);
        this.payload = payload;
    }

    public ApiResponseBody(HttpStatus status, String message) {
        super(status, message);
    }
}