package com.mouclepatrick.personalcoaching.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@JsonIgnoreProperties(value = {"coaches","programmes"})
public class Sujet implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String libelle;
    @ManyToMany(mappedBy = "sujets")
    private List<Coach> coaches;
    @ManyToMany(mappedBy = "sujets")
    private List<Programme> programmes;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Sujet))
            return false;
        Sujet sujet = (Sujet) o;
        return id.equals(sujet.id) && Objects.equals(libelle, sujet.libelle) && Objects.equals(programmes, sujet.programmes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, libelle);
    }
}