package com.mouclepatrick.personalcoaching.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.time.LocalTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@AllArgsConstructor
@DiscriminatorValue("coach")
public class Coach extends Utilisateur {
    @OneToMany(mappedBy = "coach", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Reservation> reservations;
    // lors de la suppression d'un coach garder les sujets
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinTable(name = "utilisateur_sujet", joinColumns = { @JoinColumn(name = "coach_id") }, inverseJoinColumns = {
            @JoinColumn(name = "sujet_id") })
    private List<Sujet> sujets;
    @OneToMany(mappedBy = "coach", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Programme> programmes;
    @Column(name = "horaire_debut")
    private LocalTime horaireDebut;
    @Column(name = "horaire_fin")
    private LocalTime horaireFin;

    public void addReservation(Reservation reservation) {
        reservations.add(reservation);
    }
}