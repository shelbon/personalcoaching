
package com.mouclepatrick.personalcoaching.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@Entity
@JsonIgnoreProperties(value = { "reservations" })
@DiscriminatorValue("client")

public class Client extends Utilisateur {
    @OneToMany(mappedBy = "client",cascade = CascadeType.REMOVE,orphanRemoval = true)
    private List<Reservation> reservations;

    @Builder
    public Client(Integer id, String nom, String prenom, String tel, String email, String password, String description, String profilePicturePath, List<Reservation> seances) {
        super(id, nom, prenom, tel, email, password, description, profilePicturePath);
        this.reservations = seances;
    }




    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Client))
            return false;
        Client client = (Client) o;
        return Objects.equals(reservations, client.reservations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), reservations);
    }

}