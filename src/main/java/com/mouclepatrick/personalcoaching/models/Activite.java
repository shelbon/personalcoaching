package com.mouclepatrick.personalcoaching.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.time.Duration;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(value = { "programme", "reservations" })
public class Activite implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String libelle;
    private String description;
    @Column(name = "no_ordre")
    private int numeroOrdre;
    @ManyToOne
    private Programme programme;
    @OneToMany(mappedBy = "activite")
    private List<Reservation> reservations;

    private Duration duree;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Activite))
            return false;
        Activite activite = (Activite) o;
        return id != null && id.equals(activite.id) && numeroOrdre == activite.numeroOrdre
                && Objects.equals(libelle, activite.libelle) && Objects.equals(description, activite.description)
                && programme.equals(activite.programme) && Objects.equals(reservations, activite.reservations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, libelle, description, numeroOrdre, programme, reservations);
    }
}