
package com.mouclepatrick.personalcoaching.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import java.io.Serializable;
import java.util.Locale;
import java.util.Objects;

import static javax.persistence.DiscriminatorType.STRING;
import static javax.persistence.InheritanceType.SINGLE_TABLE;


@Entity
@NoArgsConstructor
@Inheritance(strategy=SINGLE_TABLE)
@AllArgsConstructor
@DiscriminatorColumn(name="TYPE", discriminatorType=STRING)
public  abstract class Utilisateur implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nom;
    private String prenom;

    private String tel;
    @Column(unique = true)
    private String email;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    private String description;
    @Column(name = "profile_picture")
    private String profilePicturePath;

    public String getRole() {
        return  this.getClass().getSimpleName().toLowerCase(Locale.ROOT);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utilisateur)) return false;
        Utilisateur utilisateur = (Utilisateur) o;
        return Objects.equals(id, utilisateur.id) && Objects.equals(nom, utilisateur.nom) && Objects.equals(prenom, utilisateur.prenom)  && Objects.equals(tel, utilisateur.tel) && Objects.equals(email, utilisateur.email)  && Objects.equals(password, utilisateur.password) && Objects.equals(description, utilisateur.description) && Objects.equals(profilePicturePath, utilisateur.profilePicturePath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, prenom, tel, email, password, description, profilePicturePath);
    }

    public Integer getId() {
        return this.id;
    }
    public String getFullName() {
        return this.getNom() + " " + this.getPrenom();
    }
    public String getNom() {
        return this.nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public String getTel() {
        return this.tel;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPassword() {
        return this.password;
    }

    public String getDescription() {
        return this.description;
    }

    public String getProfilePicturePath() {
        return this.profilePicturePath;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }


    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public void setPassword(String password) {
        this.password = password;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setProfilePicturePath(String profilePicturePath) {
        this.profilePicturePath = profilePicturePath;
    }
}