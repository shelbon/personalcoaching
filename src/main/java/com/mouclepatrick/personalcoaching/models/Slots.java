package com.mouclepatrick.personalcoaching.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mouclepatrick.personalcoaching.utils.SlotsSerializer;

import java.time.LocalDateTime;
import java.util.List;

@JsonSerialize(using = SlotsSerializer.class)
public class Slots {
    private final List<LocalDateTime> dates;

    public Slots(List<LocalDateTime> dates) {
        this.dates = dates;
    }

    public void addDate(LocalDateTime date) {
        dates.add(date);
    }

    public List<LocalDateTime> getDates() {
        return dates;
    }
}