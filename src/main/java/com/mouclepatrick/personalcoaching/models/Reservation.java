package com.mouclepatrick.personalcoaching.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Objects;


@NoArgsConstructor
@Entity
@Getter
@Setter
@AllArgsConstructor
@JsonIgnoreProperties(value = {"client","coach","activites"})
public class Reservation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String description;
    @Column(name="date_seance")
    private LocalDate dateSeance;
    @Column(name="heure_seance")
    private LocalTime heureSeance;
    @Column(name="date_reservation")
    private LocalDateTime dateReservation;
    @Column(name = "date_annulation")
    private LocalDate dateAnnulation;
    @Column(name = "etat_seance")
    private String  etatSeance;
    private String appreciation;
    private String url;
    @ManyToOne
    @JoinColumn(name="client_id")
    private Client client;
    @ManyToOne
    @JoinColumn(name="coach_id")
    private Coach coach;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Activite activite;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Reservation)) return false;
        Reservation reservation = (Reservation) o;
        return Objects.equals(id, reservation.id) && Objects.equals(description, reservation.description) && Objects.equals(dateSeance, reservation.dateSeance) && Objects.equals(heureSeance, reservation.heureSeance) && Objects.equals(dateReservation, reservation.dateReservation) && Objects.equals(dateAnnulation, reservation.dateAnnulation) && Objects.equals(etatSeance, reservation.etatSeance) && Objects.equals(appreciation, reservation.appreciation) && Objects.equals(client, reservation.client) && Objects.equals(coach, reservation.coach) && Objects.equals(activite, reservation.activite);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, dateSeance, heureSeance, dateReservation, dateAnnulation, etatSeance, appreciation, client, coach, activite);
    }

    public String getFullDateString() {
        return dateSeance +" "+heureSeance;
    }

    public enum Etat{
        PENDING,
        ACCEPTED,
        PERFORMED,
        CANCELED,
        REFUSED
    }
}