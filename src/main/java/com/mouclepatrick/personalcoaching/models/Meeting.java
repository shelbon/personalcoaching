package com.mouclepatrick.personalcoaching.models;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Setter
@Getter
public class Meeting {
    LocalDate date;

    Slots slots;

    public Meeting(LocalDate date, Slots slots) {
        this.date = date;
        this.slots = slots;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Meeting))
            return false;
        Meeting meeting = (Meeting) o;
        return date.equals(meeting.date) && slots.equals(meeting.slots);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, slots);
    }

    @Override
    public String toString() {
        return "Meeting{" +
                "date=" + date +
                ", slots=" + slots +
                '}';
    }

    public void addDate(LocalDateTime date) {
        slots.addDate(date);
    }

}