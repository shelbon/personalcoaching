package com.mouclepatrick.personalcoaching.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;

@Getter
@Setter
public class ProgrammeCancellationDTO {
    @Min(value = 1,message = "coach doesn't exist")
    private final Integer coachId;
    @Min(1)
    private final Integer programmeId;

    public ProgrammeCancellationDTO(int coachId, int programmeId) {
        this.coachId = coachId;
        this.programmeId = programmeId;
    }
}