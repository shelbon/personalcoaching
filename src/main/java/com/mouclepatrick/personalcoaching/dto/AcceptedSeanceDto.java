package com.mouclepatrick.personalcoaching.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class AcceptedSeanceDto implements Serializable {
    @NotNull
    private final Integer id;
    @NotBlank
    private final String url;
    @NotNull
    private final Integer coachId;
}