package com.mouclepatrick.personalcoaching.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.ArrayList;

@Getter
@Setter
@SuperBuilder
public class CoachProfileDTO extends BaseProfileDTO {
    private String heureDebut;
    @NotEmpty(message = "Au moins un sujets est requis")
    private ArrayList<SujetDTO> sujets;
    private String heureFin;
    @NotBlank(message = "Your description is required")
    @Size(max = 300, message = "300 caractère max")
    private String description;

    public CoachProfileDTO(Integer id, String userType, @NotBlank(message = "nom should not be empty") String nom, @NotBlank(message = " prenom should not be empty") String prenom, @NotBlank(message = "email should not be empty") String email, MultipartFile imageProfile, String heureDebut, ArrayList<SujetDTO> sujets, String heureFin, String description) {
        super(id, userType, nom, prenom, email, imageProfile);
        this.heureDebut = heureDebut;
        this.sujets = sujets;
        this.heureFin = heureFin;
        this.description = description;
    }

    public ArrayList<SujetDTO> getSujets() {
        if (sujets == null) {
            return new ArrayList<>();
        }
        return sujets;
    }

    public CoachProfileDTO() {
        super();
    }

}