package com.mouclepatrick.personalcoaching.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.Duration;

@Data
public class ActiviteDTO implements Serializable {
    private final Integer id;
    @NotBlank(message = "should not be empty")
    private final String libelle;
    @NotBlank(message = "should not be empty")
    private final String description;
    @Min(1)
    private final int numeroOrdre;
    private final Duration duree;
}