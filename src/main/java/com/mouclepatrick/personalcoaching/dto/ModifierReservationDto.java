package com.mouclepatrick.personalcoaching.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ModifierReservationDto {
    @NotNull
    private Integer id;
    @NotNull
    private  Integer userId;
    @FutureOrPresent
    private LocalDateTime newDate;
}