package com.mouclepatrick.personalcoaching.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SujetDTO implements Serializable {
    private   Integer id;
    @NotBlank(message = "libelle could not be empty")
    private   String libelle;
}