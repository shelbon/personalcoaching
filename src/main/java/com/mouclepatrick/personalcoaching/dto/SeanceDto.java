package com.mouclepatrick.personalcoaching.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@AllArgsConstructor
public class SeanceDto implements Serializable {
    private Integer id;
    private LocalDate dateSeance;
    private LocalTime heureSeance;
    private String etatSeance;
    private ClientDto client;
    private String coachName;
    private Integer coachId;
    private final String programmeName;
    private final ReservationDetailsDto.ActiviteDto activiteDto;
}