package com.mouclepatrick.personalcoaching.dto;

import com.mouclepatrick.personalcoaching.controllers.validator.ValidEmail;
import com.mouclepatrick.personalcoaching.controllers.validator.ValidPassword;
import com.mouclepatrick.personalcoaching.response.Dto;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
@Data
public class UserLoginDTO implements Serializable, Dto {
    @ValidEmail(message = "username not valid")
    private final String email;
    @NotNull
    @ValidPassword
    private final String password;
}