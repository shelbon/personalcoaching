package com.mouclepatrick.personalcoaching.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.Transient;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class BaseProfileDTO implements Serializable {
    private Integer id;

    private String userType;
    @NotBlank(message = "nom should not be empty")
    private String nom;
    @NotBlank(message = " prenom should not be empty")
    private String prenom;
    @NotBlank(message = "email should not be empty")
    private String email;
    @Transient
    private MultipartFile imageProfile;

}