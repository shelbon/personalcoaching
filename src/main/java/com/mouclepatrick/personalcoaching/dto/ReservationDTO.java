package com.mouclepatrick.personalcoaching.dto;

import lombok.Getter;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Min;
import java.time.LocalDateTime;
@Getter
public class ReservationDTO {
    @Min(value = 1)
    private int activiteId;
    @Min(value = 1)
    private int programmeId;
    @Min(value = 1)
    private int clientId;
    @Min(value = 1)
    private int coachId;
    @FutureOrPresent
    private LocalDateTime date;

    public ReservationDTO(int activiteId, int programmeId, int clientId, int coachId, LocalDateTime date) {
        this.activiteId = activiteId;
        this.programmeId = programmeId;
        this.clientId = clientId;
        this.coachId = coachId;
        this.date = date;
    }
}