package com.mouclepatrick.personalcoaching.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
public class ReservationDetailsDto implements Serializable {
    private final Integer id;
    private final LocalDate dateSeance;
    private final LocalTime heureSeance;
    private  final  String programmeName;
    private final LocalDateTime dateReservation;
    private final ClientDto client;
    private final String coachNom;
    private final String coachPrenom;
    private final ActiviteDto activite;

    @Data
    public static class ActiviteDto implements Serializable {
        private final Integer id;
        private final String libelle;
        private final String description;
        private final String duree;
    }
}