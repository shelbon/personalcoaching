package com.mouclepatrick.personalcoaching.dto;


import com.mouclepatrick.personalcoaching.controllers.validator.UniqueNumeroOrdreConstraint;
import com.mouclepatrick.personalcoaching.controllers.validator.ValidDateProgramme;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Builder
@ValidDateProgramme
public class ProgrammeDto implements Serializable {
    private final Integer id;
    @Min(value = 1,message = "coach not found")
    private final int coachId;
    @DateTimeFormat(pattern = "yyyy-MM-DD")
    private final LocalDate dateDebut;
    @FutureOrPresent(message = "date de fin ne peut pas être dans le passé")
    private final LocalDate dateFin;
    @DateTimeFormat(pattern = "yyyy-MM-DD")
    private final String dateAnnulation;
    @NotBlank(message = "Le nom ne peut pas être vide")
    private final String nom;
    @Valid
    @NotEmpty(message = "Au moins un sujet est requis.")
    private final List<SujetDTO> sujets;
    @Valid
    @UniqueNumeroOrdreConstraint
    @NotEmpty(message = "Au moins une activite est requis.")
    private final List<ActiviteDTO> activites;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProgrammeDto)) return false;
        ProgrammeDto that = (ProgrammeDto) o;
        return Objects.equals(id, that.id) && coachId == that.coachId && Objects.equals(dateDebut, that.dateDebut) && Objects.equals(dateFin, that.dateFin) && Objects.equals(dateAnnulation, that.dateAnnulation) && Objects.equals(nom, that.nom) && Objects.equals(sujets, that.sujets) && Objects.equals(activites, that.activites);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, coachId, dateDebut, dateFin, dateAnnulation, nom, sujets, activites);
    }
}