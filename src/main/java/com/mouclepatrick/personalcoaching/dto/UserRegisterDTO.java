package com.mouclepatrick.personalcoaching.dto;

import com.mouclepatrick.personalcoaching.controllers.validator.FieldsValueMatch;
import com.mouclepatrick.personalcoaching.controllers.validator.OneOf;
import com.mouclepatrick.personalcoaching.controllers.validator.ValidEmail;
import com.mouclepatrick.personalcoaching.controllers.validator.ValidPassword;
import com.mouclepatrick.personalcoaching.response.Dto;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalTime;

@Data
@FieldsValueMatch.List({
        @FieldsValueMatch(
                field = "password",
                fieldMatch = "confirmPassword",
                message = "Passwords do not match!"
        )
})
public class UserRegisterDTO implements Serializable, Dto {
    @ValidEmail(message = "email not valid")
    private final String email;
    @NotNull
    @ValidPassword
    private final String password;
    @OneOf(comparator = {"client", "coach", "admin"})
    private String userType;
    @NotNull
    private String nom;
    @NotNull
    private String prenom;
    @NotNull
    @ValidPassword
    private String confirmPassword;
    private String description;
    private MultipartFile profilePicturePath;
    private LocalTime horaireDebut;
    private LocalTime horaireFin;
}