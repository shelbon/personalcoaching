package com.mouclepatrick.personalcoaching.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mouclepatrick.personalcoaching.models.Programme;
import com.mouclepatrick.personalcoaching.models.Reservation;
import com.mouclepatrick.personalcoaching.models.Sujet;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;
import java.util.List;

@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoachDTO {
    private final Integer id;
    private final String nom;
    private final String prenom;
    private final String email;
    private final String description;
    private final LocalTime horaireDebut;
    private final LocalTime horaireFin;
    private final List<Sujet> sujets;
    private final String imageProfile;
    private final List<Reservation> seances;
    private final List<Programme> programmes;

}