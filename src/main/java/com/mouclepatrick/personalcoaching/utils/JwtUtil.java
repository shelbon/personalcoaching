package com.mouclepatrick.personalcoaching.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.InvalidClaimException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.mouclepatrick.personalcoaching.response.ApiResponseBody;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.mouclepatrick.personalcoaching.utils.ResponseUtility.createJSONResponseContent;


@Component
@PropertySource("classpath:jwt.properties")
public class JwtUtil {

    @Value("${jwt.token.expiration}")
    public long EXPIRATION_TIME;
    @Value("${jwt.refresh.token.expiration}")
    public long REFRESH_EXPIRATION_TIME;
    @Value("${jwt.secret.key}")
    public String SECRET_KEY;
    public static final String PREFIX = "Bearer ";
    public static final String AUTH_HEADER = "Authorization";
    public static final String REFRESH_TOKEN_ENDPOINT = "/api/auth/refresh-token";
    public static final String COOKIE_BEARER = "COOKIE-BEARER";
    public static final String REFRESH_COOKIE = "REFRESH-COOKIE";

    public String generateAccessToken(Algorithm algorithm, HttpServletRequest request, Map<String, Object> data) {
        JWTCreator.Builder jwtBuilder = JWT.create();
        data.forEach((key, value) -> {
            if ("subject".equals(key)) {
                jwtBuilder.withSubject((String) value);
            } else if ("claims".equals(key) && value.getClass().isInstance(new HashMap<String, String>())) {
                ((HashMap<String, String>) value).forEach(jwtBuilder::withClaim);
            }
        });
        return jwtBuilder.withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .withIssuer(request.getRequestURL().toString()).sign(algorithm);
    }

    public String generateRefreshToken(Algorithm algorithm, String subject, HttpServletRequest request) {
        return JWT.create().withSubject(subject)
                .withExpiresAt(new Date(System.currentTimeMillis() + REFRESH_EXPIRATION_TIME))
                .withIssuer(request.getRequestURL().toString())
                .sign(algorithm);
    }

    public Optional<Cookie> getRefreshTokenCookie(HttpServletRequest request) {
        return Arrays.stream(request.getCookies()).filter(cookie -> cookie.getName().equals(REFRESH_COOKIE))
                .findFirst();
    }

    public Optional<Cookie> getAccessTokenCookie(HttpServletRequest request) {
        if (request.getCookies() != null) {
            return Arrays.stream(request.getCookies()).filter(cookie -> cookie.getName().equals(COOKIE_BEARER))
                    .findFirst();
        }
        return Optional.empty();

    }

    /**
     * Perform the verification against the given Token, using any previous configured options.
     *
     * @param token to verify.
     * @return a verified and decoded JWT.
     * @throws AlgorithmMismatchException     if the algorithm stated in the token's header is not equal to the one defined in the {@link JWTVerifier}.
     * @throws SignatureVerificationException if the signature is invalid.
     * @throws TokenExpiredException          if the token has expired.
     * @throws InvalidClaimException          if a claim contained a different value than the expected one.
     */
    public static DecodedJWT verifyToken(Algorithm algorithm, String token) {
        JWTVerifier jwtVerifier = JWT.require(algorithm).build();
        return jwtVerifier.verify(token);
    }

    public static void createJWTresponse(HttpServletResponse response, String jwtAccessToken, String refreshToken) throws IOException {
        Map<String, String> idToken = new HashMap<>();
        idToken.put("access-token", jwtAccessToken);
        idToken.put("refresh-token", refreshToken);
        response.setHeader(AUTH_HEADER, jwtAccessToken);
        createJSONResponseContent(response, idToken, HttpStatus.OK);
    }

    public static ApiResponseBody<Object> createJWTresponse(String jwtAccessToken, String refreshToken) {
        Map<String, String> idToken = new HashMap<>();
        idToken.put("access-token", jwtAccessToken);
        idToken.put("refresh-token", refreshToken);
        return new ApiResponseBody<>(HttpStatus.OK, idToken);
    }


}