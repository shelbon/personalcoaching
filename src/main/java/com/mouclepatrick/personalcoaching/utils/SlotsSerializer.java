package com.mouclepatrick.personalcoaching.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.mouclepatrick.personalcoaching.models.Slots;

import io.sentry.Sentry;

import java.io.IOException;

public class SlotsSerializer extends JsonSerializer<Slots> {
    @Override
    public void serialize(Slots slots, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartArray();
        slots.getDates().forEach(date -> {
            try {
                gen.writeStartObject();
                gen.writeObjectField("date", date);
                gen.writeEndObject();
            } catch (IOException error) {
                Sentry.captureException(error);
            }

        });
        gen.writeEndArray();
    }
}