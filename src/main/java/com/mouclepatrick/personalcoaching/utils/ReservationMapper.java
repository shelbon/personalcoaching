
package com.mouclepatrick.personalcoaching.utils;

import com.mouclepatrick.personalcoaching.dto.ClientDto;
import com.mouclepatrick.personalcoaching.dto.ReservationDetailsDto;
import com.mouclepatrick.personalcoaching.dto.SeanceDto;
import com.mouclepatrick.personalcoaching.models.Activite;
import com.mouclepatrick.personalcoaching.models.Reservation;
import org.apache.commons.lang3.time.DurationFormatUtils;


 public class ReservationMapper {
    private ReservationMapper() {
    }

    public static SeanceDto toSeanceDto(Reservation seance) {
        if (seance == null) {
            throw new IllegalArgumentException("source should be a valid object");
        }
        String programmeName = seance.getActivite().getProgramme().getNom();
        Activite activite=seance.getActivite();
        ReservationDetailsDto.ActiviteDto activiteDTOS = new ReservationDetailsDto.ActiviteDto(activite.getId(),activite.getLibelle(), activite.getDescription(), DurationFormatUtils.formatDurationWords(activite.getDuree().toMillis(),true,true));

        return new SeanceDto(seance.getId(), seance.getDateSeance(), seance.getHeureSeance(), seance.getEtatSeance(),new ClientDto(seance.getClient().getNom(), seance.getClient().getPrenom()),seance.getCoach().getFullName(),seance.getCoach().getId(), programmeName,activiteDTOS);
    }

    public static ReservationDetailsDto toReservationResultDto(Reservation seance) {
        if (seance == null) {
            throw new IllegalArgumentException("source should be a valid object");
        }
        ClientDto clientDto = new ClientDto(seance.getClient().getNom(), seance.getClient().getPrenom());
        String programmeName = seance.getActivite().getProgramme().getNom();
        Activite activite=seance.getActivite();
        ReservationDetailsDto.ActiviteDto activiteDTOS = new ReservationDetailsDto.ActiviteDto(activite.getId(),activite.getLibelle(), activite.getDescription(),  DurationFormatUtils.formatDurationWords(activite.getDuree().toMillis(),true,true));
        return new ReservationDetailsDto(seance.getId(), seance.getDateSeance(), seance.getHeureSeance(), programmeName, seance.getDateReservation(), clientDto, seance.getCoach().getNom(), seance.getCoach().getPrenom(), activiteDTOS);
    }


}