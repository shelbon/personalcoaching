package com.mouclepatrick.personalcoaching.utils;

import com.mouclepatrick.personalcoaching.controllers.exception.ResourceNotFoundException;
import com.mouclepatrick.personalcoaching.dto.BaseProfileDTO;
import com.mouclepatrick.personalcoaching.dto.CoachProfileDTO;
import com.mouclepatrick.personalcoaching.dto.SujetDTO;
import com.mouclepatrick.personalcoaching.models.Client;
import com.mouclepatrick.personalcoaching.models.Coach;
import com.mouclepatrick.personalcoaching.models.Sujet;
import com.mouclepatrick.personalcoaching.repositories.SujetRepository;
import com.mouclepatrick.personalcoaching.repositories.UtilisateurRepository;
import com.mouclepatrick.personalcoaching.services.ImageService;

import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class ProfileMapper {


    private ProfileMapper() {

    }

    public static BaseProfileDTO convertToBaseProfileDto(Client entity) {
        return BaseProfileDTO.builder()
                .id(entity.getId())
                .nom(entity.getNom())
                .email(entity.getEmail())
                .imageProfile(null)
                .prenom(entity.getPrenom())
                .build();
    }


    public static Client toClient(BaseProfileDTO dto,UtilisateurRepository utilisateurRepository,ImageService imageService) {
        if (dto.getId() == null) {
            throw new IllegalArgumentException("illegal source ");
        }
        Client client = (Client) utilisateurRepository.findById(dto.getId()).orElse(null);
        if (client == null) {
            throw new ResourceNotFoundException(dto.getId(),Client.class);
        }
        if (dto.getImageProfile() != null && !dto.getImageProfile().isEmpty()) {
            try {
                imageService.saveProfilePicture(client, dto.getImageProfile().getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        client.setId(dto.getId());
        client.setNom(dto.getNom());
        client.setPrenom(dto.getPrenom());
        client.setEmail(dto.getEmail());
        return client;
    }
    public  static Coach toCoach(CoachProfileDTO coachProfileDTO, SujetRepository sujetRepository,UtilisateurRepository utilisateurRepository,ImageService imageService){
        List<Sujet> sujets = new ArrayList<>();
        if (coachProfileDTO.getId() == null) {
            throw new IllegalArgumentException("illegal source ");
        }
        Coach coach =(Coach) utilisateurRepository.findById(coachProfileDTO.getId()).orElse(null);
        if(coach==null){
            throw new ResourceNotFoundException(coachProfileDTO.getId(),Coach.class);
        }
        if (coachProfileDTO.getImageProfile() != null && !coachProfileDTO.getImageProfile().isEmpty()) {
            try {
                imageService.saveProfilePicture(coach, coachProfileDTO.getImageProfile().getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        coach.setNom(coachProfileDTO.getNom());
        coach.setPrenom(coachProfileDTO.getPrenom());
        for (SujetDTO sujetDTO : coachProfileDTO.getSujets()) {
            final Integer sujetId = sujetDTO.getId();
            if (sujetId != null) {
                sujetRepository.findById(sujetDTO.getId())
                        .ifPresent(sujets::add);
            } else {
                sujets.add(new Sujet(null, sujetDTO.getLibelle(), List.of(coach), null));
            }
        }

        if (coachProfileDTO.getHeureDebut() != null && coachProfileDTO.getHeureFin() != null) {
            coach.setHoraireDebut(LocalTime.parse(coachProfileDTO.getHeureDebut()));
            coach.setHoraireFin(LocalTime.parse(coachProfileDTO.getHeureFin()));
        }
        coach.setSujets(sujets);
        coach.setDescription(coachProfileDTO.getDescription());
        coach.setEmail(coachProfileDTO.getEmail());
        return coach;

    }
}