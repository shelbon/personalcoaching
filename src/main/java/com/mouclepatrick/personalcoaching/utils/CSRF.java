package com.mouclepatrick.personalcoaching.utils;

public class CSRF {
    private CSRF(){}
    /**
     * The name of the header carrying the CSRF token, expected in CSRF-protected requests to the server.
     */
    public static final String REQUEST_HEADER_NAME = "X-XSRF-TOKEN";
    public static final  String CSRF_COOKIE="XSRF-TOKEN";
}