package com.mouclepatrick.personalcoaching.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class ResponseUtility {
    private ResponseUtility() {
        throw new IllegalStateException("Utility class");
    }

    public static void createJSONResponseContent(HttpServletResponse res, Map<String, String> response, HttpStatus status) throws IOException {
        res.setStatus(status.value());
        res.setContentType(MediaType.APPLICATION_JSON_VALUE);
        res.setContentType("application/json");
        res.setCharacterEncoding("UTF-8");
        new ObjectMapper().writeValue(res.getOutputStream(), response);

    }
}