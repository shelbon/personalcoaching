package com.mouclepatrick.personalcoaching.utils;

public enum Autorites {
    CLIENT,
    COACH,
    ADMIN
}