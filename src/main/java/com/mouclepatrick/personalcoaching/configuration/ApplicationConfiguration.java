package com.mouclepatrick.personalcoaching.configuration;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.List;

@Configuration
@PropertySource("classpath:application.properties")
@Getter
public class ApplicationConfiguration {
    @Value("${api.prefix}")
    private String apiPrefix;
    @Value("${app.mode}")
    private String appEnvMode;
    @Value("${cors.allowed.origins}")
    private List<String> corsAllowedOrigins;
}
