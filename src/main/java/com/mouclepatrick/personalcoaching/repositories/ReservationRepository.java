package com.mouclepatrick.personalcoaching.repositories;

import com.mouclepatrick.personalcoaching.models.Coach;
import com.mouclepatrick.personalcoaching.models.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation, Integer> {


        boolean existsByIdAndCoach_Id(int seanceId, int coachId);

        boolean existsByIdAndClient_Id(int clientId, int coachId);

        boolean existsByIdAndEtatSeance(int seanceId, String etatSeance);

        @Transactional
        @Modifying
        @Query("update Reservation s set s.etatSeance = 'CANCELED' where s.id = ?1")
        int annulerSeance(Integer id);
        @Query(value = "Select  CASE when NOT EXISTS(SELECT  *  from reservation,activite act\n" +
                        "where act.id =activite_id\n" +
                        "and  date_seance = :date_seance\n" +
                        "and coach_id=:coachID\n" +
                        "and :debut_seance <= heure_seance+make_interval(secs=>act.duree/1000000000)\n" +
                        "and :fin_seance >= heure_seance\n" +
                        "and etat_seance <>'CANCELED'\n" +
                        "        ) then true\n" +
                        "        else  false\n" +
                        "end", nativeQuery = true)
        boolean isSeancePossible(@Param("coachID") int coachId, @Param("date_seance") LocalDate dateSeance,
                        @Param("debut_seance") LocalTime heureSeance, @Param("fin_seance") LocalTime endSeance);

        @Query(value = "SELECT  * from seance \n" +
                        "                                            where  date_seance = :date_seance\n" +
                        "                                              and coach_id=:coachID\n" +
                        "                                              and :heure_seance <= heure_seance+cast(cast(duree as text) as interval )\n"
                        +
                        "                                              and :end_seance >= heure_seance\n" +
                        "                                              and etat_seance <>'CANCELED'\n" +
                        "                  ", nativeQuery = true)
        Reservation getSeanceInRange(@Param("coachID") int coachId, @Param("date_seance") LocalDate dateSeance,
                                     @Param("heure_seance") LocalTime heureSeance, @Param("end_seance") LocalTime endSeance);

        List<Reservation> findSeancesByCoachAndDateSeance(Coach coach, LocalDate dateSeance);

        @Query("select s from Reservation s where s.dateSeance = current_date  and s.coach.id = :coachID and s.etatSeance ='PENDING'  ")
        List<Reservation> getTodayPendingSeances(@Param("coachID") Integer coachId);

        @Query("select s from Reservation s where s.dateSeance = CURRENT_DATE and s.coach.id = :coachID and s.etatSeance = 'ACCEPTED' and s.heureSeance between :localTime and  :endTime")
        List<Reservation> getCoachUpcomingSeances(@Param("coachID") Integer coachId, @Param("endTime") LocalTime endTime,
                                             @Param("localTime") LocalTime localTime);
        @Query("select s from Reservation s where s.dateSeance >= current_date and s.client.id = :clientId and s.etatSeance = 'ACCEPTED'")
        List<Reservation> getUpcomingSeances(@Param("clientId") Integer clientId);


        @Transactional
        @Modifying
        @Query("update Reservation s set s.etatSeance = 'REFUSED' where s.id = ?1")
        int refuserSeance(Integer seanceId);
        @Query("select  s from Reservation s where s.client.id = ?1 and s.etatSeance <>'PENDING' and s.etatSeance <> 'ACCEPTED'")
    List<Reservation> getClientPassedSeances(int cliendId);


}
