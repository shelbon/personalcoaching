package com.mouclepatrick.personalcoaching.repositories;

import com.mouclepatrick.personalcoaching.models.Programme;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProgrammeRepository extends JpaRepository<Programme,Integer> {

  List<Programme> findAllByCoach_Id(int coachId);

  Programme findProgrammeById(int programmeID);
  boolean existsProgrammeByIdAndCoachId(int programmeId, int coachId);
}