package com.mouclepatrick.personalcoaching.repositories;

import com.mouclepatrick.personalcoaching.models.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminRepository extends JpaRepository<Admin, Integer> {
}