package com.mouclepatrick.personalcoaching.repositories;

import com.mouclepatrick.personalcoaching.models.Client;
import com.mouclepatrick.personalcoaching.models.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client, Integer> {
    @Query("select s from Reservation s,Client  c where s.client.id = c.id and c.id=?1 and s.etatSeance ='PENDING' ")
    List<Reservation> getPendingSeances(int clientId);

}