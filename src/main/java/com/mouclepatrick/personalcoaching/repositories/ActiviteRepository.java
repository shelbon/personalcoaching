package com.mouclepatrick.personalcoaching.repositories;

import com.mouclepatrick.personalcoaching.models.Activite;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ActiviteRepository extends JpaRepository<Activite, Integer> {
}