package com.mouclepatrick.personalcoaching.repositories;

import com.mouclepatrick.personalcoaching.models.Coach;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoachRepository extends JpaRepository<Coach, Integer> {

}