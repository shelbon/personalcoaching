package com.mouclepatrick.personalcoaching.services;

import com.mouclepatrick.personalcoaching.abstraction.BaseService;
import com.mouclepatrick.personalcoaching.dto.BaseProfileDTO;
import com.mouclepatrick.personalcoaching.dto.CoachProfileDTO;
import com.mouclepatrick.personalcoaching.dto.UserRegisterDTO;
import com.mouclepatrick.personalcoaching.models.Utilisateur;
import com.mouclepatrick.personalcoaching.repositories.SujetRepository;
import com.mouclepatrick.personalcoaching.repositories.UtilisateurRepository;
import com.mouclepatrick.personalcoaching.utils.ProfileMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class UtilisateurService implements BaseService<Utilisateur>, Converter<Utilisateur, BaseProfileDTO> {
    private final PasswordEncoder passwordEncoder;

    private final UtilisateurRepository utilisateurRepository;
    private final ImageService imageService;
    private final SujetRepository sujetRepository;

    @Autowired
    public UtilisateurService(PasswordEncoder passwordEncoder, UtilisateurRepository utilisateurRepository, ImageService imageService, SujetRepository sujetRepository) {
        this.passwordEncoder = passwordEncoder;
        this.utilisateurRepository = utilisateurRepository;
        this.imageService = imageService;

        this.sujetRepository = sujetRepository;
    }

    @Override
    public Utilisateur add(Utilisateur user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return utilisateurRepository.save(user);
    }

    @Override
    public Utilisateur findBy(Integer id) {
        return utilisateurRepository.findById(id).orElse(null);
    }

    @Override
    public List<Utilisateur> findAll() {
        return utilisateurRepository.findAll();
    }

    @Override
    public Utilisateur update(Utilisateur entity) {
        return utilisateurRepository.save(entity);
    }

    public boolean userExist(UserRegisterDTO userRegisterDTO) {
        return utilisateurRepository.existsPersonneByEmail(userRegisterDTO.getEmail());
    }

    public Utilisateur getByEmail(String email) {
        return utilisateurRepository.findByEmail(email);
    }

    @Override
    public Utilisateur convertToEntity(BaseProfileDTO dto) {
        if (dto.getId() == null) {
            throw new IllegalArgumentException("illegal source");
        }
        Utilisateur utilisateur = utilisateurRepository.findById(dto.getId()).orElse(null);
        if (utilisateur == null) {
            throw new IllegalArgumentException("user  doesn't exist");
        }
        if (dto.getImageProfile() != null && !dto.getImageProfile().isEmpty()) {
            try {
                imageService.saveProfilePicture(utilisateur, dto.getImageProfile().getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        utilisateur.setId(dto.getId());
        utilisateur.setNom(dto.getNom());
        utilisateur.setPrenom(dto.getPrenom());
        utilisateur.setEmail(dto.getEmail());
        return utilisateur;
    }

    public Utilisateur convertToEntity(CoachProfileDTO dto) {
        return ProfileMapper.toCoach(dto, sujetRepository, utilisateurRepository, imageService);
    }

    @Override
    public BaseProfileDTO convertToDto(Utilisateur source) {

        if (source == null) {
            throw new IllegalArgumentException("user can't be null");
        }
        return BaseProfileDTO.builder().id(source.getId())
                .nom(source.getNom())
                .prenom(source.getPrenom())
                .userType(source.getRole())
                .email(source.getEmail())
                .imageProfile(null)
                .build();


    }

    public void deleteById(Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("id can't be null");
        }
        Utilisateur utilisateur = findBy(id);

        utilisateurRepository.delete(utilisateur);
    }
}