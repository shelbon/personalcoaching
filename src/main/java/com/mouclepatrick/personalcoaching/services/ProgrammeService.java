package com.mouclepatrick.personalcoaching.services;

import com.mouclepatrick.personalcoaching.controllers.exception.ResourceNotFoundException;
import com.mouclepatrick.personalcoaching.dto.ActiviteDTO;
import com.mouclepatrick.personalcoaching.dto.ProgrammeDto;
import com.mouclepatrick.personalcoaching.dto.SujetDTO;
import com.mouclepatrick.personalcoaching.models.Activite;
import com.mouclepatrick.personalcoaching.models.Coach;
import com.mouclepatrick.personalcoaching.models.Programme;
import com.mouclepatrick.personalcoaching.models.Reservation;
import com.mouclepatrick.personalcoaching.models.Sujet;
import com.mouclepatrick.personalcoaching.repositories.ActiviteRepository;
import com.mouclepatrick.personalcoaching.repositories.CoachRepository;
import com.mouclepatrick.personalcoaching.repositories.ProgrammeRepository;
import com.mouclepatrick.personalcoaching.repositories.ReservationRepository;
import com.mouclepatrick.personalcoaching.repositories.SujetRepository;
import io.sentry.Sentry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ProgrammeService implements Converter<Programme, ProgrammeDto> {
    private final ProgrammeRepository programmeRepository;
    private final SujetRepository sujetRepository;
    private final ActiviteRepository activiteRepository;
    private final CoachRepository coachRepository;

    @Autowired
    public ProgrammeService(ProgrammeRepository programmeRepository, ReservationRepository reservationRepository,
            SujetRepository sujetRepository, ActiviteRepository activiteRepository, CoachRepository coachRepository) {
        this.programmeRepository = programmeRepository;
        this.sujetRepository = sujetRepository;
        this.activiteRepository = activiteRepository;
        this.coachRepository = coachRepository;
    }

    public List<ProgrammeDto> getAllProgrammeDTOByCoach(int coachID) {
        return programmeRepository
                .findAllByCoach_Id(coachID)
                .stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    /**
     * @param id identifiant du programme a recuperer
     * @return le programme demander ou null si il n'existe pas
     * @throws IllegalArgumentException si id de l'entité est null
     */
    public ProgrammeDto getProgrammeDTO(int id) {
        Programme programme = programmeRepository.findProgrammeById(id);
        if (programme == null) {
            throw new ResourceNotFoundException(id, Programme.class);
        }
        return convertToDto(programme);
    }

    public Programme getProgramme(int id) {
        return programmeRepository.findById(id).orElse(null);
    }

    /**
     * sauvegarde le programme et le retourne
     *
     * @param nouveauProgramme nouveau programme - ne droit pas être null
     * @return le programme créer
     * @throws IllegalArgumentException in case the given programme is null.
     */
    public Programme createProgramme(Programme nouveauProgramme) {
        return programmeRepository.save(nouveauProgramme);
    }

    public void deleteProgramme(int id) {
        try {
            programmeRepository.deleteById(id);
        } catch (EmptyResultDataAccessException error) {
            Sentry.captureException(error);
            throw new EmptyResultDataAccessException(Objects.requireNonNull(error.getMessage()), 0);
        }

    }

    /**
     * Annule un programme et les seances qui n'ont pas etait effecutées
     *
     * @param id identifiant du programme a annuller
     * @throws IllegalArgumentException dans le cas que l'entité est null
     * @throws EntityNotFoundException  dans le cas que l'entité n'existe pas
     */
    public Programme cancelProgramme(int id) {

        try {
            Programme programme = getProgramme(id);

            for (Activite activite : programme.getActivites()) {
                for (Reservation seance : activite.getReservations()) {
                    if (seance.getEtatSeance().contains(Reservation.Etat.PENDING.toString())
                            || seance.getEtatSeance().contains(Reservation.Etat.ACCEPTED.toString())) {
                        seance.setDateAnnulation(LocalDate.now());
                        seance.setEtatSeance(Reservation.Etat.CANCELED.toString());
                    }
                }

            }
            programme.setDateAnnulation(LocalDate.now().toString());
            programmeRepository.save(programme);
            return programme;
        } catch (IllegalArgumentException error) {
            Sentry.captureException(error);
            throw new IllegalArgumentException(error.getMessage());
        } catch (NullPointerException error) {
            Sentry.captureException(error);
            throw new NullPointerException(error.getMessage());
        }

    }

    public Programme updateProgramme(Programme existingProgramme) {
        return programmeRepository.save(existingProgramme);
    }

    public boolean isCoachProgramme(int coachId, int programmeId) {
        return programmeRepository.existsProgrammeByIdAndCoachId(programmeId, coachId);
    }

    @Override
    public Programme convertToEntity(ProgrammeDto dto) {
        if (dto == null) {
            throw new IllegalArgumentException("source conversion can\'t be null ");
        }
        Programme programme = new Programme();
        List<Sujet> sujets = new ArrayList<>();
        List<Activite> activites = new ArrayList<>();
        Coach coach = coachRepository.findById(dto.getCoachId()).orElse(null);
        if (coach == null) {
            throw new ResourceNotFoundException(dto.getCoachId(), Coach.class);
        }
        programme.setCoach(coach);
        dto.getSujets().forEach(sujetDTO -> {
            final Integer sujetId = sujetDTO.getId();
            if (sujetId != null) {
                sujetRepository.findById(sujetDTO.getId())
                        .ifPresent(sujets::add);
            } else {
                sujets.add(new Sujet(null, sujetDTO.getLibelle(), List.of(coach), List.of(programme)));
            }

        });
        dto.getActivites().forEach(activiteDTO -> {
            if (activiteDTO.getId() != null) {
                activiteRepository.findById(activiteDTO.getId())
                        .ifPresent((activite -> {
                            activite.setDescription(activiteDTO.getDescription());
                            activite.setLibelle(activiteDTO.getLibelle());
                            activite.setNumeroOrdre(activiteDTO.getNumeroOrdre());
                            activite.setDuree(activiteDTO.getDuree());
                            activites.add(activite);
                        }));
            } else {
                Activite activite = new Activite();
                activite.setProgramme(programme);
                activite.setLibelle(activiteDTO.getLibelle());
                activite.setDescription(activiteDTO.getDescription());
                activite.setNumeroOrdre(activiteDTO.getNumeroOrdre());
                activite.setDuree(activiteDTO.getDuree());
                activite.setReservations(null);
                activites.add(activite);
            }

        });
        if (dto.getId() != null) {
            programme.setId(dto.getId());
        }
        programme.setNom(dto.getNom());
        programme.setDateAnnulation(dto.getDateAnnulation());
        programme.setSujets(sujets);
        programme.setActivites(activites);
        programme.setDateDebut(dto.getDateDebut());
        programme.setDateFin(dto.getDateFin());
        return programme;
    }

    @Override
    public ProgrammeDto convertToDto(Programme entity) {
        List<ActiviteDTO> activiteDTO = entity.getActivites()
                .stream()
                .map(activite -> new ActiviteDTO(activite.getId(), activite.getLibelle(), activite.getDescription(),
                        activite.getNumeroOrdre(), activite.getDuree()))
                .collect(Collectors.toList());
        List<SujetDTO> sujetDTO = entity.getSujets().stream()
                .map(sujet -> new SujetDTO(sujet.getId(), sujet.getLibelle())).collect(Collectors.toList());
        return ProgrammeDto.builder()
                .id(entity.getId())
                .coachId(entity.getCoach().getId())
                .activites(activiteDTO)
                .dateAnnulation(entity.getDateAnnulation())
                .dateDebut(entity.getDateDebut())
                .dateFin(entity.getDateFin())
                .nom(entity.getNom())
                .sujets(sujetDTO)
                .build();
    }

}