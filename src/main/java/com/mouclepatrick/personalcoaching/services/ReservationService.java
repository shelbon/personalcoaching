package com.mouclepatrick.personalcoaching.services;

import com.mouclepatrick.personalcoaching.abstraction.BaseService;
import com.mouclepatrick.personalcoaching.dto.ReservationDetailsDto;
import com.mouclepatrick.personalcoaching.models.Reservation;
import com.mouclepatrick.personalcoaching.repositories.CoachRepository;
import com.mouclepatrick.personalcoaching.repositories.ReservationRepository;
import com.mouclepatrick.personalcoaching.utils.ReservationMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Service
public class ReservationService implements BaseService<Reservation> {
    private final ReservationRepository reservationRepository;
    private final CoachRepository coachRepository;

    public ReservationService(ReservationRepository reservationRepository, CoachRepository coachRepository) {
        this.reservationRepository = reservationRepository;
        this.coachRepository = coachRepository;
    }

    @Override
    public Reservation add(Reservation entity) {
        return reservationRepository.save(entity);
    }

    @Override
    public Reservation findBy(Integer id) {
        return reservationRepository.findById(id).orElse(null);
    }

    @Override
    public List<Reservation> findAll() {
        return reservationRepository.findAll();
    }

    @Override
    public Reservation update(Reservation entity) {
        return reservationRepository.save(entity);
    }

    /**
     * verifie la validité d'une seance
     *
     * @param coachId
     * @param dateSeance
     * @return vrai si les informations d'une seance sont correcte
     * @throws IllegalArgumentException quand le coachId est null
     */
    private boolean isReservationValid(int coachId, LocalDate dateSeance) {
        if (dateSeance.isBefore(LocalDate.now())) {
            return false;
        }
        return coachRepository.existsById(coachId);
    }

    /**
     * Verifie si une reservation est disponible à cette date et heure pour un coach
     *
     * @param coachId     l'id du coach
     * @param dateSeance  la date de seance
     * @param heureSeance heure de la seance
     * @return vrai si une seance est disponible à cette heure sinon faux
     */
    public Boolean isReservationPossible(int coachId, LocalDate dateSeance, LocalTime heureSeance, LocalTime endTime) {
        if (isReservationValid(coachId, dateSeance)) {
            return reservationRepository.isSeancePossible(coachId, dateSeance, heureSeance, endTime);
        }
        return false;
    }

    public ReservationDetailsDto toReservationResultDto(Reservation seance) {
        return ReservationMapper.toReservationResultDto(seance);
    }
}