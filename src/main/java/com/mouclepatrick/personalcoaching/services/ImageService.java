package com.mouclepatrick.personalcoaching.services;

import com.mouclepatrick.personalcoaching.models.Utilisateur;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

//TODO (sparkles)add possibility to add and retrieve from cloudinary
@Service
public class ImageService {
    @Value("${profiles.img.path}")
    String path;
    Logger logger = LoggerFactory.getLogger(ImageService.class);

    public int saveProfilePicture(Utilisateur p, InputStream fi) {
        String oldProfilePicturePath=p.getProfilePicturePath();
        if(oldProfilePicturePath!=null){
            removePhoto(oldProfilePicturePath);
        }
        p.setProfilePicturePath(save("p", "personnes", fi));
        return 0;
    }

     public boolean removePhoto(String fileName) {
        String subPath="personnes";
        Path filePath =Path.of(path + "/" + subPath + "/" + fileName);
        try {
            return Files.deleteIfExists(filePath);
        } catch (IOException e) {
            Sentry.captureException(e);
            return false;
        }
    }


    private String save(String prefix, String subPath, InputStream fi) {
        String fileName = "";
        try (DirectoryStream<Path> dir = Files.newDirectoryStream(Paths.get(path + "/" + subPath), prefix + "*")) {

            fileName = prefix + UUID.randomUUID() + ".jpg";

            String filePath = path + "/" + subPath + "/" + fileName;

            Files.copy(fi, new File(filePath).toPath());

        } catch (IOException ioe) {
            logger.error("Erreur sur nom d'image : " + ioe.getMessage());
        }


        return fileName;
    }

    public InputStream retreivePhoto(String fileName) {
        return retreiveImage("personnes", fileName);
    }


    /**
     * get image  FileInputStream: subpath/fileName
     *
     * @param subPath  directory name
     * @param fileName of image
     * @return FileInputStream of image or null if failure
     */
    private InputStream retreiveImage(String subPath, String fileName) {
        InputStream is = null;
        try {
            is = new FileInputStream(path + "/" + subPath + "/" + fileName);
        } catch (FileNotFoundException fnfe) {
            logger.error(String.format("Erreur récupération de l'image %s : %s", fileName, fnfe.getMessage()));
        }
        return is;
    }
}