package com.mouclepatrick.personalcoaching.services;

import com.mouclepatrick.personalcoaching.abstraction.BaseService;
import com.mouclepatrick.personalcoaching.dto.SeanceDto;
import com.mouclepatrick.personalcoaching.models.Client;
import com.mouclepatrick.personalcoaching.models.Reservation;
import com.mouclepatrick.personalcoaching.repositories.ClientRepository;
import com.mouclepatrick.personalcoaching.repositories.ReservationRepository;
import com.mouclepatrick.personalcoaching.utils.ReservationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientService implements BaseService<Client> {
    private final ClientRepository clientRepository;

    private final ReservationRepository reservationRepository;

    @Autowired
    public ClientService(ClientRepository clientRepository, ReservationRepository reservationRepository) {
        this.clientRepository = clientRepository;

        this.reservationRepository = reservationRepository;
    }


    @Override
    public Client add(Client entity) {
        return clientRepository.save(entity);
    }

    @Override
    public Client findBy(Integer id) {
        return clientRepository.findById(id).orElse(null);
    }

    public List<Client> findAll() {
        return clientRepository.findAll();
    }

    @Override
    public Client update(Client entity) {
        return clientRepository.save(entity);
    }


    public List<SeanceDto> getPendingSeances(int clientId) {
        return clientRepository.getPendingSeances(clientId)
                .stream()
                .map(ReservationMapper::toSeanceDto)
                .collect(Collectors.toList());
    }

    public boolean reservationExiste(int reservationId, int coachId) {
        return reservationRepository.existsByIdAndClient_Id(reservationId, coachId);
    }

    public boolean isReservationPending(Integer reservationId) {
        return reservationRepository.existsByIdAndEtatSeance(reservationId, Reservation.Etat.PENDING.toString());
    }

    public int canceledReservation(Integer reservationId) {
        return reservationRepository.annulerSeance(reservationId);
    }

    public List<SeanceDto> getPassedReservations(Integer clientId) {
        if (clientId == null) {
            throw new IllegalArgumentException("clientId not right");
        }
        return  reservationRepository.getClientPassedSeances(clientId).stream()
                .map(ReservationMapper::toSeanceDto)
                .collect(Collectors.toList());
    }

    public List<SeanceDto> getUpcomingReservations(Integer clientId) {
        return reservationRepository.getUpcomingSeances(clientId)
                .stream()
                .map(ReservationMapper::toSeanceDto)
                .collect(Collectors.toList());
    }
}