package com.mouclepatrick.personalcoaching.services;

import com.mouclepatrick.personalcoaching.abstraction.BaseService;
import com.mouclepatrick.personalcoaching.dto.BaseProfileDTO;
import com.mouclepatrick.personalcoaching.models.Admin;
import com.mouclepatrick.personalcoaching.repositories.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class AdminService implements BaseService<Admin>, Converter<Admin, BaseProfileDTO> {
    private final AdminRepository adminRepository;
    private final ImageService imageService;

    @Autowired
    public AdminService(AdminRepository adminRepository, ImageService imageService) {
        this.adminRepository = adminRepository;
        this.imageService = imageService;
    }

    @Override
    public Admin add(Admin entity) {
        return adminRepository.save(entity);
    }

    @Override
    public Admin findBy(Integer id) {
        return adminRepository.findById(id).orElse(null);
    }

    @Override
    public List<Admin> findAll() {
        return adminRepository.findAll();
    }

    @Override
    public Admin update(Admin entity) {
        return adminRepository.save(entity);
    }

    @Override
    public Admin convertToEntity(BaseProfileDTO dto) {
        if (dto.getId() == null) {
            throw new IllegalArgumentException("illegal source");
        }
        Admin admin = adminRepository.findById(dto.getId()).orElse(null);
        if (admin == null) {
            throw new IllegalArgumentException("Admin  doesn't exist");
        }
        if (dto.getImageProfile() != null && !dto.getImageProfile().isEmpty()) {
            try {
                imageService.saveProfilePicture(admin, dto.getImageProfile().getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        admin.setId(dto.getId());
        admin.setNom(dto.getNom());
        admin.setPrenom(dto.getPrenom());
        admin.setEmail(dto.getEmail());
        return admin;
    }

    @Override
    public BaseProfileDTO convertToDto(Admin source) {

        if (source == null) {
            throw new IllegalArgumentException("admin can't be null");
        }
        return BaseProfileDTO.builder().id(source.getId())
                .nom(source.getNom())
                .prenom(source.getPrenom())
                .email(source.getEmail())
                .imageProfile(null)
                .build();


    }
}