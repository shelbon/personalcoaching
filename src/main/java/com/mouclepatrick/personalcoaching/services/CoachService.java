package com.mouclepatrick.personalcoaching.services;

import com.mouclepatrick.personalcoaching.controllers.exception.ResourceNotFoundException;
import com.mouclepatrick.personalcoaching.dto.CoachDTO;
import com.mouclepatrick.personalcoaching.dto.CoachProfileDTO;
import com.mouclepatrick.personalcoaching.dto.SeanceDto;
import com.mouclepatrick.personalcoaching.dto.SujetDTO;
import com.mouclepatrick.personalcoaching.models.Coach;
import com.mouclepatrick.personalcoaching.models.Reservation;
import com.mouclepatrick.personalcoaching.models.Sujet;
import com.mouclepatrick.personalcoaching.repositories.CoachRepository;
import com.mouclepatrick.personalcoaching.repositories.ReservationRepository;
import com.mouclepatrick.personalcoaching.repositories.SujetRepository;
import com.mouclepatrick.personalcoaching.utils.ReservationMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CoachService implements Converter<Coach, CoachProfileDTO>,
        org.springframework.core.convert.converter.Converter<Reservation, SeanceDto> {
    private final CoachRepository coachRepository;
    private final ReservationRepository reservationRepository;
    private final ImageService imageService;
    private final SujetRepository sujetRepository;

    public CoachService(CoachRepository coachRepository, ReservationRepository reservationRepository,
                        ImageService imageService, SujetRepository sujetRepository) {
        this.coachRepository = coachRepository;
        this.reservationRepository = reservationRepository;
        this.imageService = imageService;
        this.sujetRepository = sujetRepository;
    }

    public Coach getCoach(int id) {
        return coachRepository.findById(id).orElse(null);
    }

    public List<Coach> getCoaches() {
        return coachRepository.findAll();
    }

    public Coach addCoach(Coach coach) {
        return coachRepository.save(coach);
    }

    /***
     *
     * @param coach
     * @return updated coach
     * @throws ResourceNotFoundException when coach doesn't exist.
     */
    public Coach updateCoach(Coach coach) {
        boolean coachExist = coachRepository.existsById(coach.getId());
        if (coachExist) {
            return coachRepository.save(coach);
        }
        throw new ResourceNotFoundException(coach.getId(), Coach.class);

    }

    public void deleteCoach(int id) {
        coachRepository.deleteById(id);
    }

    public Coach updateCoach(CoachProfileDTO coachProfileDTO) {
        Coach coach = convertToEntity(coachProfileDTO);
        return coachRepository.save(coach);
    }

    @Override
    public Coach convertToEntity(CoachProfileDTO source) {
        Coach coach = new Coach();
        List<Sujet> sujets = new ArrayList<>();

        if (source.getId() != null) {
            coach = this.getCoach(source.getId());
        }
        if (coach.getId() == null) {
            throw new IllegalArgumentException("source should be a valid object");
        }
        if (source.getImageProfile() != null && !source.getImageProfile().isEmpty()) {
            try {
                imageService.saveProfilePicture(coach, source.getImageProfile().getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        coach.setNom(source.getNom());
        coach.setPrenom(source.getPrenom());

        for (SujetDTO sujetDTO : source.getSujets()) {
                final Integer sujetId = sujetDTO.getId();
                if (sujetId != null) {
                    sujetRepository.findById(sujetDTO.getId())
                            .ifPresent(sujets::add);
                } else {
                    sujets.add(new Sujet(null, sujetDTO.getLibelle(), List.of(coach), null));
                }
            }



        if (source.getHeureDebut() != null && source.getHeureFin() != null) {
            coach.setHoraireDebut(LocalTime.parse(source.getHeureDebut()));
            coach.setHoraireFin(LocalTime.parse(source.getHeureFin()));
        }
        coach.setSujets(sujets);
        coach.setDescription(source.getDescription());
        coach.setEmail(source.getEmail());
        return coach;

    }

    @Override
    public CoachProfileDTO convertToDto(Coach source) {
        CoachProfileDTO coachProfileDTO = new CoachProfileDTO();
        if (source == null) {
            throw new IllegalArgumentException("coach can't be null");
        }
        if (source.getHoraireDebut() != null) {
            coachProfileDTO.setHeureDebut(source.getHoraireDebut().toString());
        }
        if (source.getHoraireFin() != null) {
            coachProfileDTO.setHeureFin(source.getHoraireFin().toString());
        }
        coachProfileDTO.setSujets((ArrayList<SujetDTO>) source.getSujets().stream()
                .map(sujet -> new SujetDTO(sujet.getId(), sujet.getLibelle())).collect(Collectors.toList()));
        coachProfileDTO.setId(source.getId());
        coachProfileDTO.setNom(source.getNom());
        coachProfileDTO.setUserType(source.getRole());
        coachProfileDTO.setDescription(source.getDescription());
        coachProfileDTO.setEmail(source.getEmail());
        coachProfileDTO.setImageProfile(null);
        coachProfileDTO.setPrenom(source.getPrenom());

        return coachProfileDTO;

    }

    public CoachDTO entityToCoachDTO(Coach coach) {
        if (coach == null) {
            throw new IllegalArgumentException("coach can't be null");
        }
        return CoachDTO.builder()
                .id(coach.getId())
                .nom(coach.getNom())
                .prenom(coach.getPrenom())
                .sujets(coach.getSujets())
                .imageProfile(coach.getProfilePicturePath())
                .build();
    }

    public List<Reservation> getTodayPendingSeances(Integer coachId) {
        return reservationRepository.getTodayPendingSeances(coachId);
    }

    public List<Reservation> getUpcomingSeances(Integer coachId) {
        return reservationRepository.getCoachUpcomingSeances(coachId, LocalTime.now().plusHours(5), LocalTime.now());
    }

    @Override

    public SeanceDto convert(Reservation seance) {

        return ReservationMapper.toSeanceDto(seance);
    }

    public Reservation accepterSeance(Reservation seance) {
        if (seance == null) {
            throw new IllegalArgumentException("seance can't be null ");
        }
        seance.setEtatSeance(Reservation.Etat.ACCEPTED.toString());
        return reservationRepository.save(seance);

    }

    public int refuserSeance(Integer seanceId) {
        if (seanceId == null) {
            throw new IllegalArgumentException("seance can't be null ");
        }
        return reservationRepository.refuserSeance(seanceId);
    }

    public boolean seanceExiste(int seanceId, int coachId) {
        return reservationRepository.existsByIdAndCoach_Id(seanceId, coachId);
    }

    public boolean isSeancePending(Integer seanceId) {
        return reservationRepository.existsByIdAndEtatSeance(seanceId, Reservation.Etat.PENDING.toString());
    }

}
