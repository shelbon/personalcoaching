package com.mouclepatrick.personalcoaching.services;

import com.mouclepatrick.personalcoaching.controllers.exception.DateRangeException;
import com.mouclepatrick.personalcoaching.models.Coach;
import com.mouclepatrick.personalcoaching.models.Meeting;
import com.mouclepatrick.personalcoaching.models.Slots;
import com.mouclepatrick.personalcoaching.repositories.ReservationRepository;
import de.jollyday.HolidayCalendar;
import de.jollyday.HolidayManager;
import de.jollyday.ManagerParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ScheduleService {

    private final ReservationRepository reservationRepository;

    private final Duration INTERVAL = Duration.ofMinutes(30);

    @Autowired
    public ScheduleService(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;

    }

    /**
     * get range of date between start and end date
     *
     * @param startDate start date
     * @param endDate   end date
     * @return range of date between start and end date
     * @throws DateRangeException when  date range invalid
     */
    private List<LocalDate> getDays(LocalDate startDate, LocalDate endDate) {
        if (!endDate.isAfter(startDate) && !startDate.isBefore(endDate)) {
            throw new DateRangeException("illegal date range");
        }
        ArrayList<LocalDate> days = new ArrayList<>();
        LocalDate newDate = startDate;
        for (int day = startDate.getDayOfMonth(); newDate.isBefore(endDate); day++) {
            if (day != startDate.getDayOfMonth()) {
                newDate = newDate.plusDays(1);
            }
            if (isPublicHolidays(newDate)) {
                continue;
            }
            days.add(newDate);

        }
        return days;
    }

    /**
     * get  possible reservation hour
     *
     * @param startDate   start date
     * @param coach       a coach
     * @param interval    interval between hour
     * @return range of date between start and end date
     * @throws IllegalArgumentException when coach is null
     */
    private List<LocalDateTime> getHours(Coach coach, LocalDate startDate, Duration interval) {
        if (coach == null) {
            throw new IllegalArgumentException("jourDispos should not be null");
        }
        var hours = new ArrayList<LocalDateTime>();


        var endDateTime = startDate.atTime(coach.getHoraireFin());
        for (LocalDateTime dateTime = startDate.atTime(coach.getHoraireDebut()); dateTime.isBefore(endDateTime); dateTime = dateTime.plus(interval)) {
            boolean isSeancePossible = reservationRepository.isSeancePossible(coach.getId(), dateTime.toLocalDate(), dateTime.toLocalTime(), dateTime.toLocalTime().plus(interval));
            if (isSeancePossible) {
                hours.add(dateTime);
            }
        }
        return hours;
    }

    private boolean isPublicHolidays(LocalDate date) {
        HolidayManager holidayManager = HolidayManager.getInstance(ManagerParameters.create(HolidayCalendar.FRANCE));
        return holidayManager.isHoliday(date);

    }


    public List<Meeting> getMeetings(Coach coach, LocalDate startDate, LocalDate endDate, Duration activiteDuree) {
        var days = getDays(startDate, endDate);
        List<Meeting> meetings = new ArrayList<>();
        Duration interval;
        if (INTERVAL.compareTo(activiteDuree) >= 0) {
            interval = INTERVAL;
        } else {
            interval = activiteDuree;
        }

        for (LocalDate day : days) {
            var slots = new Slots(getHours(coach, day, interval));

            meetings.add(new Meeting(day, slots));
        }
        meetings.sort((meeting, meeting2) -> {
            if (meeting.getDate().isBefore(meeting2.getDate()))
                return -1;
            else if (meeting.getDate().isAfter(meeting2.getDate())) {
                return +1;
            } else
                return 0;
        });
        return meetings;
    }
}