package com.mouclepatrick.personalcoaching.services;

public interface Converter<S, T> {
    S convertToEntity(T dto);

    T convertToDto(S entity);
}