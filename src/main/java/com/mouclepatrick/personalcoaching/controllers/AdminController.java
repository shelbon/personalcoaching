package com.mouclepatrick.personalcoaching.controllers;

import com.mouclepatrick.personalcoaching.dto.BaseProfileDTO;
import com.mouclepatrick.personalcoaching.models.Admin;
import com.mouclepatrick.personalcoaching.models.UserPrincipal;
import com.mouclepatrick.personalcoaching.response.ApiResponse;
import com.mouclepatrick.personalcoaching.services.AdminService;
import com.mouclepatrick.personalcoaching.services.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class AdminController {
    private final AdminService adminService;
    private final UtilisateurService utilisateurService;

    @Autowired
    public AdminController(AdminService adminService, UtilisateurService utilisateurService) {
        this.adminService = adminService;
        this.utilisateurService = utilisateurService;
    }

    @PostMapping(value = "/admins/{id}/profile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<ApiResponse> profile(@Valid @ModelAttribute BaseProfileDTO userProfileDTO,
            HttpSession session) {
        Admin modifiedAdmin = adminService.convertToEntity(userProfileDTO);
        adminService.update(modifiedAdmin);
        ApiResponse apiResponse = new ApiResponse(HttpStatus.OK, "profile modify successfully");
        return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
    }

    @DeleteMapping("/users/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<ApiResponse> deleteUser(@PathVariable Integer id, Authentication authentication) {
        ApiResponse apiResponse;
        UserPrincipal currentUser = (UserPrincipal) authentication.getPrincipal();
        String currentUserRole = currentUser.getAuthorities().stream().findFirst().get().getAuthority();
        if (!id.equals(currentUser.getId()) && !currentUserRole.equals("admin")) {
            apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, "You can't delete someone else");
            return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
        }
        if (currentUserRole.equals("admin") && id.equals(currentUser.getId())) {
            apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, "You can't delete yourself");
            return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
        }
        utilisateurService.deleteById(id);
        apiResponse = new ApiResponse(HttpStatus.OK, "User deleted successfully");
        return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
    }

}