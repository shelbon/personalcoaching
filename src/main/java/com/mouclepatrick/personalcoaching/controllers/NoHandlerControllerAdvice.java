package com.mouclepatrick.personalcoaching.controllers;

import com.google.common.io.Files;
import org.apache.commons.io.IOUtils;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.io.IOException;
import java.io.InputStreamReader;

@ControllerAdvice
public class NoHandlerControllerAdvice {
    @Value("${spa.default-file}")
    String defaultFile;


    private final ResourceLoader resourceLoader;

    @Autowired
    public NoHandlerControllerAdvice(ResourceLoader resourceLoader) {

        this.resourceLoader = resourceLoader;
    }

    @Bean
    private Tika tika() {
        return new Tika();
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<Object> renderDefaultPage(NoHandlerFoundException exc) {
        StringBuilder filePath = new StringBuilder();
        Resource resource;
        filePath.append("classpath:/public");
        try {
            filePath.append(exc.getRequestURL());
            String extension = Files.getFileExtension(filePath.toString());

            if (!extension.isBlank()) {
                resource = resourceLoader.getResource(filePath.toString());

            } else {
                resource = resourceLoader.getResource(defaultFile);
            }
            String mimeType =tika().detect(resource.getInputStream(),resource.getFilename());


            Object body;
            if (mimeType.startsWith("image")) {
                body = IOUtils.toByteArray(resource.getInputStream());
            } else {
                body = IOUtils.toString(new InputStreamReader(resource.getInputStream()));
            }


            return ResponseEntity.ok().contentType(MediaType.valueOf(mimeType)).body(body);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("There was an error completing the action.");
        }
    }


}