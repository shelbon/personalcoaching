package com.mouclepatrick.personalcoaching.controllers;

import com.mouclepatrick.personalcoaching.models.Utilisateur;
import com.mouclepatrick.personalcoaching.services.ImageService;
import com.mouclepatrick.personalcoaching.services.UtilisateurService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.Date;


@RestController
@RequestMapping(value = "/api/img")
public class ImageController {
    private UtilisateurService utilisateurService;
    private ImageService imageService;

    @Autowired
    public ImageController(UtilisateurService utilisateurService, ImageService imageService) {
        this.utilisateurService = utilisateurService;
        this.imageService = imageService;
    }


    @GetMapping(value = "/users/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> photos(@PathVariable("id") int id, Authentication authentication) {
        byte[] image = null;
        Utilisateur user = utilisateurService.findBy(id);
        if (user != null) {
            String photoName = user.getProfilePicturePath();
            if (photoName != null) {
                InputStream is = imageService.retreivePhoto(photoName);
                if (is != null) {
                    try {
                        image = IOUtils.toByteArray(is);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        }
        HttpHeaders headers = new HttpHeaders();
        headers.set("Last-Modified", DateFormat.getDateTimeInstance().format(new Date()));
        return  ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.IMAGE_JPEG)
                .body(image);

    }
}