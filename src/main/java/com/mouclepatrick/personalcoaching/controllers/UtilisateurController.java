package com.mouclepatrick.personalcoaching.controllers;

import com.mouclepatrick.personalcoaching.dto.BaseProfileDTO;
import com.mouclepatrick.personalcoaching.dto.CoachProfileDTO;
import com.mouclepatrick.personalcoaching.models.Coach;
import com.mouclepatrick.personalcoaching.models.UserPrincipal;
import com.mouclepatrick.personalcoaching.models.Utilisateur;
import com.mouclepatrick.personalcoaching.response.ApiResponse;
import com.mouclepatrick.personalcoaching.response.ApiResponseBody;
import com.mouclepatrick.personalcoaching.services.ClientService;
import com.mouclepatrick.personalcoaching.services.CoachService;
import com.mouclepatrick.personalcoaching.services.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import static com.mouclepatrick.personalcoaching.utils.Autorites.COACH;

import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/api")
public class UtilisateurController {
    private final UtilisateurService utilisateurService;
    private final CoachService coachService;

    @Autowired
    public UtilisateurController(UtilisateurService utilisateurService, ClientService clientService,
            CoachService coachService) {
        this.utilisateurService = utilisateurService;
        this.coachService = coachService;
    }

    @GetMapping("/users")
    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<ApiResponseBody<List<Utilisateur>>> getAll() {
        ApiResponseBody<List<Utilisateur>> apiResponse = new ApiResponseBody<>(HttpStatus.OK,
                utilisateurService.findAll());
        return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
    }

    @GetMapping("/users/{id}/profile")
    @PreAuthorize("hasAnyAuthority('admin','client','coach')")
    public ResponseEntity<ApiResponse> profile(@PathVariable Integer id, Authentication authentication) {
        ApiResponse apiResponse;
        BaseProfileDTO profileDTO;
        UserPrincipal currentUser = (UserPrincipal) authentication.getPrincipal();
        String currentUserRole = currentUser.getAuthorities().stream().findFirst().get().getAuthority();
        if (!id.equals(currentUser.getId()) && !currentUserRole.equals("admin")) {
            apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, "it's not your profile");
            return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
        }
        Utilisateur utilisateur = utilisateurService.findBy(id);
        if (utilisateur == null) {
            throw new EntityNotFoundException("utilisateur not found for id:" + id);
        }
        if (utilisateur.getRole().equals(COACH.toString().toLowerCase(Locale.ROOT))) {
            profileDTO = coachService.convertToDto((Coach) utilisateur);
        } else {
            profileDTO = utilisateurService.convertToDto(utilisateur);
        }
        apiResponse = new ApiResponseBody<>(HttpStatus.OK, profileDTO);
        return new ResponseEntity<>(apiResponse, apiResponse.getStatus());

    }

    @PostMapping(value = "/users/{id}/profile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @PreAuthorize("hasAnyAuthority('admin','client')")
    public ResponseEntity<ApiResponse> profile(@Valid BaseProfileDTO baseProfileDTO, Authentication authentication) {
        ApiResponse apiResponse;
        UserPrincipal currentUser = (UserPrincipal) authentication.getPrincipal();

        String currentUserRole = currentUser.getAuthorities().stream().findFirst().get().getAuthority();
        if (!baseProfileDTO.getId().equals(currentUser.getId()) && !currentUserRole.equals("admin")) {
            apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, "it's not your profile");
            return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
        }
        Utilisateur user = utilisateurService.convertToEntity(baseProfileDTO);

        utilisateurService.update(user);
        apiResponse = new ApiResponse(HttpStatus.OK, "profile modify successfully");
        return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
    }

    @PostMapping(value = "/users/{id}/profile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, params = {
            "userType=coach" })
    @PreAuthorize("hasAnyAuthority('admin','coach')")
    public ResponseEntity<ApiResponse> profile(@Valid CoachProfileDTO coachProfileDTO, Authentication authentication) {
        ApiResponse apiResponse;
        UserPrincipal currentUser = (UserPrincipal) authentication.getPrincipal();
        String currentUserRole = currentUser.getAuthorities().stream().findFirst().get().getAuthority();
        if (!coachProfileDTO.getId().equals(currentUser.getId()) && !currentUserRole.equals("admin")) {
            apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, "it's not your profile");
            return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
        }
        Utilisateur user = utilisateurService.convertToEntity(coachProfileDTO);

        utilisateurService.update(user);
        apiResponse = new ApiResponse(HttpStatus.OK, "profile modify successfully");
        return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
    }
}