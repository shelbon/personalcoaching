package com.mouclepatrick.personalcoaching.controllers;

import com.auth0.jwt.exceptions.TokenExpiredException;
import com.mouclepatrick.personalcoaching.controllers.exception.DateRangeException;
import com.mouclepatrick.personalcoaching.controllers.exception.ResourceNotFoundException;
import com.mouclepatrick.personalcoaching.response.ApiError;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@ControllerAdvice

public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    /**
     * Handle MethodArgumentNotValidException. Triggered when an object fails @Valid validation.
     *
     * @param ex      the MethodArgumentNotValidException that is thrown when @Valid validation fails
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        ApiError apiError = new ApiError(BAD_REQUEST);
        apiError.setMessage("Validation error");
        apiError.addValidationErrors(ex.getBindingResult().getFieldErrors());
        apiError.addValidationError(ex.getBindingResult().getGlobalErrors());
        return buildResponseEntity(apiError);
    }
    /**
     * Handle handleBindException. Triggered when an object fails @Valid validation with @ModelAttribute
     *
     * @param ex      the MethodArgumentNotValidException that is thrown when @Valid validation fails
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ApiError apiError = new ApiError(BAD_REQUEST);
        apiError.setMessage("Validation error");
        apiError.addValidationErrors(ex.getBindingResult().getFieldErrors());
        apiError.addValidationError(ex.getBindingResult().getGlobalErrors());
        return buildResponseEntity(apiError);
    }

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }
    /**
     * Handle MaxUploadSizeExceededException. Triggered when an upload file exceeded  upload limit.
     *
     * @param exc      the MaxUploadSizeExceededException
     * @return the ApiError object
     */
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<Object> handleMaxSizeException(MaxUploadSizeExceededException exc) {
        ApiError apiError = new ApiError(BAD_REQUEST);
        apiError.setMessage(exc.getMessage());
        return buildResponseEntity(apiError);
    }
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Object>   handleCoachNotFoundException(ResourceNotFoundException exc){
        ApiError apiError = new ApiError(NOT_FOUND);
        apiError.setMessage(exc.getSimpleMessage());
        apiError.setDebugMessage(exc.getMessage());
        return buildResponseEntity(apiError);
    }
    @ExceptionHandler(DateRangeException.class)
    public ResponseEntity<Object>   handleUserAlreadyExistException(DateRangeException exc){
        ApiError apiError = new ApiError(BAD_REQUEST);
        apiError.setMessage(exc.getMessage());
        return buildResponseEntity(apiError);
    }
    @ExceptionHandler(TokenExpiredException.class)
    public  ResponseEntity<Object> handleTokenExpiredException(TokenExpiredException  exc){
        ApiError apiError = new ApiError(UNAUTHORIZED);
        apiError.setMessage("Session expired please reconnect");
        apiError.setDebugMessage(exc.getMessage());
        return buildResponseEntity(apiError);
    }

}