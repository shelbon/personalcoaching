package com.mouclepatrick.personalcoaching.controllers;

import com.mouclepatrick.personalcoaching.controllers.exception.ResourceNotFoundException;
import com.mouclepatrick.personalcoaching.dto.AcceptedSeanceDto;
import com.mouclepatrick.personalcoaching.dto.CoachDTO;
import com.mouclepatrick.personalcoaching.dto.ProgrammeCancellationDTO;
import com.mouclepatrick.personalcoaching.dto.ProgrammeDto;
import com.mouclepatrick.personalcoaching.dto.SeanceDto;
import com.mouclepatrick.personalcoaching.models.Client;
import com.mouclepatrick.personalcoaching.models.Coach;
import com.mouclepatrick.personalcoaching.models.Reservation;
import com.mouclepatrick.personalcoaching.repositories.ReservationRepository;
import com.mouclepatrick.personalcoaching.response.ApiResponse;
import com.mouclepatrick.personalcoaching.response.ApiResponseBody;
import com.mouclepatrick.personalcoaching.services.CoachService;
import com.mouclepatrick.personalcoaching.services.EmailService;
import com.mouclepatrick.personalcoaching.services.ProgrammeService;
import com.mouclepatrick.personalcoaching.services.ReservationService;
import io.sentry.Sentry;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class CoachController {

    private final CoachService coachService;
    private final ReservationService reservationService;
    private final ProgrammeService programmeService;
    private final EmailService emailService;
    @Value("${mail.from}")
    private String mailSender;

    @Autowired
    public CoachController(CoachService coachService, ReservationRepository reservationRepository,
            ReservationService reservationService,
            ProgrammeService programmeService, EmailService emailService) {
        this.coachService = coachService;
        this.reservationService = reservationService;
        this.programmeService = programmeService;
        this.emailService = emailService;
    }

    @GetMapping("/coaches")
    public ApiResponseBody<Object> all() {
        List<Coach> coaches = coachService.getCoaches();
        try {
            List<CoachDTO> coachDTOS = coaches.stream().map(coachService::entityToCoachDTO)
                    .collect(Collectors.toUnmodifiableList());
            return new ApiResponseBody<>(HttpStatus.OK, coachDTOS);

        } catch (Exception e) {
            Sentry.captureException(e);
        }
        return new ApiResponseBody<>(HttpStatus.OK, Collections.emptyList());
    }

    @GetMapping("/coaches/{id}")
    public CoachDTO coach(@PathVariable int id) {
        Coach coach = coachService.getCoach(id);
        if (coach != null) {
            CoachDTO coachDTO = null;
            try {
                coachDTO = CoachDTO.builder().id(coach.getId())
                        .id(coach.getId())
                        .nom(coach.getNom())
                        .prenom(coach.getPrenom())
                        .sujets(coach.getSujets())
                        .imageProfile(coach.getProfilePicturePath())
                        .horaireDebut(coach.getHoraireDebut())
                        .horaireFin(coach.getHoraireFin())
                        .programmes(coach.getProgrammes())
                        .build();
            } catch (Exception e) {
                Sentry.captureException(e);
            }
            return coachDTO;
        }
        throw new ResourceNotFoundException(id,Coach.class);

    }

    @GetMapping("/coaches/{id}/programmes")
    @PreAuthorize("hasAnyAuthority('coach','admin')")
    public List<ProgrammeDto> programmes(@PathVariable("id") int coachID) {

        return programmeService.getAllProgrammeDTOByCoach(coachID);
    }

    @PutMapping("/coaches/{coachId}/programmes/{programmeId}/cancel")
    @PreAuthorize("hasAnyAuthority('coach','admin')")
    public ApiResponse cancelProgrammeCoach(@Valid ProgrammeCancellationDTO programmeCancellationDTO) {
        boolean ownTheProgramme = programmeService.isCoachProgramme(programmeCancellationDTO.getCoachId(),
                programmeCancellationDTO.getProgrammeId());
        if (ownTheProgramme) {
            programmeService.cancelProgramme(programmeCancellationDTO.getProgrammeId());
            return ApiResponse.builder()
                    .status(HttpStatus.OK)
                    .message("Programme annulé")
                    .build();
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Programme not owned by coach");

    }

    @GetMapping("/coaches/{id}/seances/valide")
    @PreAuthorize("hasAnyAuthority('coach','admin')")
    public Map<String, Object> getSeances(@PathVariable("id") Integer coachId) {
        List<SeanceDto> pendingSeances = coachService.getTodayPendingSeances(coachId).stream()
                .map((coachService::convert)).collect(Collectors.toList());
        List<SeanceDto> upcomingSeances = coachService.getUpcomingSeances(coachId).stream().map((coachService::convert))
                .collect(Collectors.toList());
        Map<String, Object> response = new HashMap<>();
        response.put("pendingSeances", pendingSeances);
        response.put("upcomingSeances", upcomingSeances);
        return response;

    }

    @PutMapping("/coaches/{coachId}/seances/{seanceId}/accepted")
    @PreAuthorize("hasAnyAuthority('coach','admin')")
    public ResponseEntity<ApiResponse> accepterSeance(@Valid @RequestBody AcceptedSeanceDto acceptedSeanceDto) {
        ApiResponse apiResponse;
        boolean seanceExiste = coachService.seanceExiste(acceptedSeanceDto.getId(), acceptedSeanceDto.getCoachId());

        if (!seanceExiste) {
            apiResponse = ApiResponse.builder().status(HttpStatus.BAD_REQUEST)
                    .message("this Seance doesn't exist for this coach").build();
            return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
        }

        boolean isSeancePending = coachService.isSeancePending(acceptedSeanceDto.getId());

        if (!isSeancePending) {
            apiResponse = ApiResponse.builder().status(HttpStatus.BAD_REQUEST)
                    .message("This Seance is not  in a pending state ").build();
            return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
        }

        Reservation seance = reservationService.findBy(acceptedSeanceDto.getId());
        seance.setUrl(acceptedSeanceDto.getUrl());
        Reservation acceptedSeance = coachService.accepterSeance(seance);
        Client client = seance.getClient();

        if (Objects.equals(acceptedSeance.getEtatSeance(), Reservation.Etat.ACCEPTED.toString())) {
            Map<String, Object> templateModel = new HashMap<>();
            templateModel.put("recipientName", client.getFullName());
            templateModel.put("text", seance.getCoach().getFullName());
            templateModel.put("programmeDate", seance.getFullDateString());
            templateModel.put("url", seance.getUrl());
            templateModel.put("activiteName", seance.getActivite().getLibelle());
            templateModel.put("programmeName", seance.getActivite().getProgramme().getNom());
            templateModel.put("activiteDuree",
                    DurationFormatUtils.formatDurationWords(seance.getActivite().getDuree().toMillis(), true, true));
            templateModel.put("senderName", mailSender);
            apiResponse = ApiResponse.builder()
                    .status(HttpStatus.OK)
                    .message("seance accepted successfully")
                    .build();
            emailService.sendMessageUsingThymeleafTemplate(client.getEmail(),
                    "Seance accepted", "seance-accepted", templateModel);
        } else {
            apiResponse = ApiResponse.builder()
                    .status(HttpStatus.BAD_REQUEST)
                    .message("unexepected error,can't accept seance")
                    .build();
        }

        return new ResponseEntity<>(apiResponse, apiResponse.getStatus());

    }

    @PutMapping("/coaches/{coachId}/seances/{seanceId}/refused")
    @PreAuthorize("hasAnyAuthority('coach','admin')")
    public ResponseEntity<ApiResponse> refuserSeance(@PathVariable Integer coachId, @PathVariable Integer seanceId) {
        boolean seanceExiste = coachService.seanceExiste(seanceId, coachId);
        ApiResponse apiResponse;
        if (!seanceExiste) {
            apiResponse = ApiResponse.builder().status(HttpStatus.BAD_REQUEST)
                    .message("this Seance doesn't exist for this coach").build();
            return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
        }
        boolean isSeancePending = coachService.isSeancePending(seanceId);

        if (!isSeancePending) {
            apiResponse = ApiResponse.builder().status(HttpStatus.BAD_REQUEST)
                    .message("This Seance is not  in a pending state ").build();
            return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
        }
        Reservation seance = reservationService.findBy(seanceId);
        Client client = seance.getClient();
        Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("recipientName", client.getFullName());
        templateModel.put("text", seance.getCoach().getFullName());
        templateModel.put("programmeDate", seance.getFullDateString());
        templateModel.put("url", seance.getUrl());
        templateModel.put("activiteName", seance.getActivite().getLibelle());
        templateModel.put("programmeName", seance.getActivite().getProgramme().getNom());
        templateModel.put("activiteDuree",
                DurationFormatUtils.formatDurationWords(seance.getActivite().getDuree().toMillis(), true, true));
        templateModel.put("senderName", mailSender);
        int hasRefused = coachService.refuserSeance(seanceId);
        if (hasRefused == 1) {
            apiResponse = ApiResponse.builder()
                    .status(HttpStatus.OK)
                    .message("seance refused successfully")
                    .build();
            emailService.sendMessageUsingThymeleafTemplate(client.getEmail(),
                    "Seance refused", "seance-refused", templateModel);
        } else {
            apiResponse = ApiResponse.builder()
                    .status(HttpStatus.BAD_REQUEST)
                    .message("unexepected error,can't refuse seance")
                    .build();
        }
        return new ResponseEntity<>(apiResponse, apiResponse.getStatus());

    }
}