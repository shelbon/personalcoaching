package com.mouclepatrick.personalcoaching.controllers.exception;

import java.text.MessageFormat;

public class ResourceNotFoundException extends RuntimeException {
    private String simpleMessage;

    public ResourceNotFoundException(int id, Class<?> entity) {
        super(MessageFormat.format("resource {0} with id  {1} was not found", entity.getSimpleName(), id));
        this.simpleMessage = MessageFormat.format("{0}  n'a pas été trouvée", entity.getSimpleName());
    }

    public String getSimpleMessage() {
        return simpleMessage;
    }
}