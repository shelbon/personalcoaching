package com.mouclepatrick.personalcoaching.controllers.exception;

public class DateRangeException extends RuntimeException {
    public DateRangeException(String message) {
        super(message);
    }
}