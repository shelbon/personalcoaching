package com.mouclepatrick.personalcoaching.controllers;

import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.mouclepatrick.personalcoaching.controllers.exception.UserAlreadyExistException;
import com.mouclepatrick.personalcoaching.dto.UserRegisterDTO;
import com.mouclepatrick.personalcoaching.models.Admin;
import com.mouclepatrick.personalcoaching.models.Client;
import com.mouclepatrick.personalcoaching.models.Coach;
import com.mouclepatrick.personalcoaching.models.Utilisateur;
import com.mouclepatrick.personalcoaching.response.ApiResponseBody;
import com.mouclepatrick.personalcoaching.services.UtilisateurService;
import com.mouclepatrick.personalcoaching.utils.JwtUtil;
import io.sentry.Sentry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import static com.mouclepatrick.personalcoaching.utils.JwtUtil.COOKIE_BEARER;
import static com.mouclepatrick.personalcoaching.utils.JwtUtil.REFRESH_COOKIE;
import static com.mouclepatrick.personalcoaching.utils.JwtUtil.verifyToken;
import static com.mouclepatrick.personalcoaching.utils.ResponseUtility.createJSONResponseContent;

@RestController
@RequestMapping(value = "/api/auth")
public class AuthController {
    private final UtilisateurService utilisateurService;
    @Resource(name = "authenticationManagerBean")
    private AuthenticationManager authManager;
    private final JwtUtil jwtUtil;

    @Autowired
    public AuthController(UtilisateurService utilisateurService, JwtUtil jwtUtil1) {
        this.utilisateurService = utilisateurService;
        this.jwtUtil = jwtUtil1;
    }

    @PostMapping("/signup")
    public ApiResponseBody<Object> signup(@Valid @RequestBody UserRegisterDTO userRegisterDTO, HttpServletRequest req,
            HttpServletResponse response) {
        if (utilisateurService.userExist(userRegisterDTO)) {
            throw new UserAlreadyExistException();
        }
        Utilisateur utilisateur;
        switch (userRegisterDTO.getUserType().toLowerCase(Locale.ROOT)) {
            case "coach":
                utilisateur = new Coach();
                break;
            case "admin":
                utilisateur = new Admin();
                break;
            case "client":
                utilisateur = new Client();
                break;
            default:
                throw new IllegalArgumentException("can't process input");
        }
        utilisateur.setEmail(userRegisterDTO.getEmail());
        utilisateur.setPassword(userRegisterDTO.getPassword());
        utilisateur.setProfilePicturePath(null);
        utilisateur.setNom(userRegisterDTO.getNom());
        utilisateur.setPrenom(userRegisterDTO.getPrenom());
        utilisateurService.add(utilisateur);

        Algorithm algorithm = Algorithm.HMAC256(jwtUtil.SECRET_KEY);
        Map<String, Object> jwtData = new HashMap<>();
        Map<String, String> claims = new HashMap<>();
        claims.put("id", utilisateur.getId().toString());
        claims.put("role", utilisateur.getRole());
        jwtData.put("subject", utilisateur.getEmail());
        jwtData.put("claims", claims);
        String jwtAccessToken = jwtUtil.generateAccessToken(algorithm, req, jwtData);
        String refreshToken = jwtUtil.generateRefreshToken(algorithm, utilisateur.getEmail(), req);
        ResponseCookie jwtCookie = ResponseCookie.from(COOKIE_BEARER, jwtAccessToken)
                .httpOnly(true)
                .secure(true)
                .path("/")
                .sameSite("Lax")
                .maxAge(jwtUtil.EXPIRATION_TIME)
                .build();
        ResponseCookie refreshCookie = ResponseCookie.from(REFRESH_COOKIE, refreshToken)
                .httpOnly(true)
                .secure(true)
                .maxAge(jwtUtil.REFRESH_EXPIRATION_TIME)
                .path(JwtUtil.REFRESH_TOKEN_ENDPOINT)
                .sameSite("Lax")
                .build();
        response.addHeader(HttpHeaders.SET_COOKIE, jwtCookie.toString());
        response.addHeader(HttpHeaders.SET_COOKIE, refreshCookie.toString());
        return new ApiResponseBody<>(HttpStatus.OK, claims);
    }

    @PostMapping("/login")
    public void login() {
    }

    @PostMapping("/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        ResponseCookie jwtCookie = ResponseCookie.from(COOKIE_BEARER, null)
                .httpOnly(true)
                .secure(true)
                .path("/")
                .sameSite("Lax")
                .maxAge(0)
                .build();
        ResponseCookie refreshCookie = ResponseCookie.from(REFRESH_COOKIE, null)
                .httpOnly(true)
                .secure(true)
                .maxAge(0)
                .path(JwtUtil.REFRESH_TOKEN_ENDPOINT)
                .sameSite("Lax")
                .build();
        response.addHeader(HttpHeaders.SET_COOKIE, jwtCookie.toString());
        response.addHeader(HttpHeaders.SET_COOKIE, refreshCookie.toString());
    }

    @GetMapping("/refresh-token")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Optional<Cookie> refreshTokenCookie = jwtUtil.getRefreshTokenCookie(request);

        if (refreshTokenCookie.isPresent()) {
            try {
                String refreshToken = refreshTokenCookie.get().getValue();
                Algorithm algorithm = Algorithm.HMAC256(jwtUtil.SECRET_KEY);
                DecodedJWT decodedJWT = verifyToken(algorithm, refreshToken);
                String email = decodedJWT.getSubject();
                // TODO check blacklist then
                Utilisateur user = utilisateurService.getByEmail(email);
                if (user != null) {
                    Map<String, Object> jwtData = new HashMap<>();
                    Map<String, String> claims = new HashMap<>();
                    claims.put("role", user.getRole());
                    claims.put("id", user.getId().toString());
                    jwtData.put("subject", user.getEmail());
                    jwtData.put("claims", claims);
                    String jwtAccessToken = jwtUtil.generateAccessToken(algorithm, request, jwtData);
                    ResponseCookie jwtCookie = ResponseCookie.from(COOKIE_BEARER, jwtAccessToken)
                            .httpOnly(true)
                            .secure(true)
                            .path("/")
                            .sameSite("Lax")
                            .maxAge(jwtUtil.EXPIRATION_TIME)
                            .build();
                    response.setHeader(HttpHeaders.SET_COOKIE, jwtCookie.toString());
                    createJSONResponseContent(response, claims, HttpStatus.OK);
                }

            } catch (Exception e) {
                Sentry.captureException(e);
                throw e;
            }

        } else {
            throw new RuntimeException("Refresh token required");
        }
    }

}