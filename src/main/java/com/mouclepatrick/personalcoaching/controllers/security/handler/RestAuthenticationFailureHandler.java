package com.mouclepatrick.personalcoaching.controllers.security.handler;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.mouclepatrick.personalcoaching.utils.ResponseUtility.createJSONResponseContent;

@Component
public class RestAuthenticationFailureHandler implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse res,
                                        AuthenticationException ex) throws IOException {

        Map<String, String> response = new HashMap<>();
        response.put("status", "42");
        response.put("message", ex.getMessage());
        createJSONResponseContent(res, response, HttpStatus.NOT_FOUND);
    }
}