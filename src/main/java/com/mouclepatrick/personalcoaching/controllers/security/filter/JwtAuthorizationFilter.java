package com.mouclepatrick.personalcoaching.controllers.security.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.mouclepatrick.personalcoaching.configuration.ApplicationConfiguration;
import com.mouclepatrick.personalcoaching.models.UserPrincipal;
import com.mouclepatrick.personalcoaching.services.JPAUserDetailsService;
import com.mouclepatrick.personalcoaching.utils.JwtUtil;
import io.sentry.Sentry;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.mouclepatrick.personalcoaching.utils.JwtUtil.REFRESH_TOKEN_ENDPOINT;
import static com.mouclepatrick.personalcoaching.utils.ResponseUtility.createJSONResponseContent;

public class JwtAuthorizationFilter extends OncePerRequestFilter {

    private final JPAUserDetailsService jpaUserDetailsService;
    private final JwtUtil jwtUtil;
    private ApplicationConfiguration applicationConfiguration;

    public JwtAuthorizationFilter(JPAUserDetailsService jpaUserDetailsService, JwtUtil jwtUtil,
            ApplicationConfiguration applicationConfiguration) {
        this.jpaUserDetailsService = jpaUserDetailsService;
        this.jwtUtil = jwtUtil;
        this.applicationConfiguration = applicationConfiguration;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        if (request.getServletPath().equals(REFRESH_TOKEN_ENDPOINT)
                || request.getServletPath().equals("/api/auth/logout")) {
            filterChain.doFilter(request, response);
        }
        Optional<Cookie> authorizationToken = jwtUtil.getAccessTokenCookie(request);
        String pathName = request.getRequestURI();
        if (authorizationToken.isPresent() && pathName.contains(applicationConfiguration.getApiPrefix())) {
            try {
                String jwt = authorizationToken.get().getValue();
                Algorithm algorithm = Algorithm.HMAC256(jwtUtil.SECRET_KEY);
                JWTVerifier jwtVerifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = jwtVerifier.verify(jwt);
                String username = decodedJWT.getSubject();
                UserPrincipal userPrincipal = (UserPrincipal) jpaUserDetailsService.loadUserByUsername(username);
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                        userPrincipal, null, userPrincipal.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                filterChain.doFilter(request, response);
            } catch (TokenExpiredException e) {
                Sentry.captureException(e);
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
            } catch (Exception e) {
                Map<String, String> responseBody = new HashMap<>();
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                responseBody.put("message", e.getLocalizedMessage());
                Sentry.captureException(e);
                createJSONResponseContent(response, responseBody, HttpStatus.FORBIDDEN);
            }

        } else {
            filterChain.doFilter(request, response);
        }

    }
}