package com.mouclepatrick.personalcoaching.controllers.security.handler;

import com.mouclepatrick.personalcoaching.services.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.mouclepatrick.personalcoaching.utils.ResponseUtility.createJSONResponseContent;

@Component
public class RestSuccessHandler implements AuthenticationSuccessHandler {
    @Autowired
    UtilisateurService utilisateurService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication authentication)
            throws IOException {
        Map<String, String> response = new HashMap<>();
        HttpSession session = req.getSession();
        String email = authentication.getName();
        int id = utilisateurService.getByEmail(email).getId();
        session.setAttribute("userId", id);
        response.put("status", "50");
        response.put("message", "Succesfull authenticate");
        response.put("id", id + "");
        res.setStatus(HttpServletResponse.SC_OK);
        createJSONResponseContent(res, response, HttpStatus.OK);
    }

}