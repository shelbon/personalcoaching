package com.mouclepatrick.personalcoaching.controllers.security.configuration;

import com.google.common.net.HttpHeaders;
import com.mouclepatrick.personalcoaching.abstraction.Environnement;
import com.mouclepatrick.personalcoaching.configuration.ApplicationConfiguration;
import com.mouclepatrick.personalcoaching.controllers.security.filter.JwtAuthenticationFilter;
import com.mouclepatrick.personalcoaching.controllers.security.filter.JwtAuthorizationFilter;
import com.mouclepatrick.personalcoaching.controllers.security.filter.StatelessCsrfFilter;
import com.mouclepatrick.personalcoaching.controllers.security.handler.RestAuthenticationEntryPoint;
import com.mouclepatrick.personalcoaching.controllers.security.handler.RestAuthenticationFailureHandler;
import com.mouclepatrick.personalcoaching.controllers.security.handler.RestSuccessHandler;
import com.mouclepatrick.personalcoaching.services.JPAUserDetailsService;
import com.mouclepatrick.personalcoaching.utils.CSRF;
import com.mouclepatrick.personalcoaching.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final JPAUserDetailsService jpaUserDetailsService;
    private final JwtUtil jwtUtil;
    private final String LOGIN_ENDPOINT = "/api/auth/login";
    private final ApplicationConfiguration applicationConfiguration;
    private StatelessCsrfFilter statelessCsrfFilter;
    @Autowired
    public WebSecurityConfig(JPAUserDetailsService jpaUserDetailsService, JwtUtil jwtUtil, ApplicationConfiguration applicationConfiguration) {
        this.jpaUserDetailsService = jpaUserDetailsService;
        this.jwtUtil = jwtUtil;
        this.applicationConfiguration = applicationConfiguration;
        this.statelessCsrfFilter = new StatelessCsrfFilter();
    }


    @Bean
    JwtAuthenticationFilter authenticationFilter() throws Exception {
        final JwtAuthenticationFilter filter = new JwtAuthenticationFilter(authenticationManagerBean(), jwtUtil, applicationConfiguration);
        filter.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(LOGIN_ENDPOINT, "POST"));
        filter.setAuthenticationSuccessHandler(successHandler());
        filter.setAuthenticationManager(authenticationManagerBean());
        filter.setAuthenticationFailureHandler(authenticationFailureHandler());
        return filter;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder delegatingPasswordEncoder() {
        PasswordEncoder defaultEncoder = new BCryptPasswordEncoder();
        Map<String, PasswordEncoder> encoders = new HashMap<>();
        encoders.put("bcrypt", new BCryptPasswordEncoder());

        DelegatingPasswordEncoder passworEncoder = new DelegatingPasswordEncoder(
                "bcrypt", encoders);
        passworEncoder.setDefaultPasswordEncoderForMatches(defaultEncoder);

        return passworEncoder;
    }

    @Bean
    RestAuthenticationFailureHandler authenticationFailureHandler() {
        return new RestAuthenticationFailureHandler();
    }

    @Bean
    RestSuccessHandler successHandler() {
        return new RestSuccessHandler();
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().authorizeRequests()
                //les  url  n'on pas besoin d'autorisation pour tout type de requete HTTP
                .antMatchers(applicationConfiguration.getApiPrefix() + "/auth/**", "/**").permitAll()
                //les  url  n'on pas besoin d'autorisation seulement pour les requetes de type GET
                .antMatchers(HttpMethod.GET, applicationConfiguration.getApiPrefix() + "/programmes/**", applicationConfiguration.getApiPrefix() + "/schedules**", applicationConfiguration.getApiPrefix() + "/coaches/**", "/assets/**").permitAll()
                //tout les autres url require d'etre authentifié
                .anyRequest().authenticated();
        //specifie a spring security de ne pas créer de session
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.exceptionHandling().authenticationEntryPoint(forbiddenEntryPoint());
        http.formLogin().loginPage(LOGIN_ENDPOINT).permitAll();
        http.addFilterBefore(statelessCsrfFilter, CsrfFilter.class);
        http.addFilterBefore(new JwtAuthorizationFilter(jpaUserDetailsService, jwtUtil, applicationConfiguration), UsernamePasswordAuthenticationFilter.class);
        http.addFilter(authenticationFilter());
        //desactive le csrf par spring security
        http.csrf().disable();


    }

    @Bean
    AuthenticationEntryPoint forbiddenEntryPoint() {
        return new RestAuthenticationEntryPoint();
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        //urls pouvant envoyer des requetes au serveur
        applicationConfiguration.getCorsAllowedOrigins().forEach(config::addAllowedOriginPattern);
        config.setMaxAge(3600L);
        config.setAllowedHeaders(List.of(HttpHeaders.ORIGIN, HttpHeaders.X_REQUESTED_WITH, HttpHeaders.CONTENT_TYPE, HttpHeaders.ACCEPT, "sentry-trace", HttpHeaders.AUTHORIZATION,"baggage",
                HttpHeaders.VIA, CSRF.REQUEST_HEADER_NAME,HttpHeaders.TE,HttpHeaders.DNT,
                HttpHeaders.SEC_FETCH_DEST,HttpHeaders.SEC_FETCH_MODE,HttpHeaders.SEC_FETCH_SITE));
        //http methode autorisé
        config.setAllowedMethods(List.of(HttpMethod.POST.toString(), HttpMethod.GET.toString(), HttpMethod.PUT.toString(), HttpMethod.OPTIONS.toString(), HttpMethod.DELETE.toString()));
        source.registerCorsConfiguration("/**", config);
        return source;
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(jpaUserDetailsService).passwordEncoder(delegatingPasswordEncoder());
    }

}
