package com.mouclepatrick.personalcoaching.controllers.security.filter;

import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mouclepatrick.personalcoaching.abstraction.Environnement;
import com.mouclepatrick.personalcoaching.configuration.ApplicationConfiguration;
import com.mouclepatrick.personalcoaching.models.UserPrincipal;
import com.mouclepatrick.personalcoaching.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseCookie;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.mouclepatrick.personalcoaching.utils.JwtUtil.COOKIE_BEARER;
import static com.mouclepatrick.personalcoaching.utils.JwtUtil.REFRESH_COOKIE;
import static com.mouclepatrick.personalcoaching.utils.ResponseUtility.createJSONResponseContent;


public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtUtil;
    private final ApplicationConfiguration applicationConfiguration;
    @Value("${spring.profiles.active:prod}")
    private String activeProfile;
    public JwtAuthenticationFilter(AuthenticationManager authenticationManager, JwtUtil jwtUtil, ApplicationConfiguration applicationConfiguration) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
        this.applicationConfiguration = applicationConfiguration;
    }

    @Override

    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse res) throws AuthenticationException {
        String username = null;
        String password = null;
        if (request.getContentType().equals(MediaType.APPLICATION_JSON_VALUE)) {
            try {
                UsernamePasswordRequest usernamePasswordRequest = new ObjectMapper().readValue(request.getInputStream(), UsernamePasswordRequest.class);
                username = usernamePasswordRequest.get(getUsernameParameter());
                password = usernamePasswordRequest.get(getPasswordParameter());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        } else {
            username = request.getParameter("username");
            password = request.getParameter("password");
        }

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        return authenticationManager.authenticate(authenticationToken);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException {
        UserPrincipal user = (UserPrincipal) authResult.getPrincipal();
        Algorithm algorithm = Algorithm.HMAC256(jwtUtil.SECRET_KEY);
        Map<String, Object> jwtData = new HashMap<>();
        Map<String, String> claims = new HashMap<>();
        user.getAuthorities().stream().findFirst().ifPresent(grantedAuthority -> claims.put("role", grantedAuthority.getAuthority()));
        claims.put("id", user.getId().toString());
        jwtData.put("subject", user.getUsername());
        jwtData.put("claims", claims);
        String jwtAccessToken = jwtUtil.generateAccessToken(algorithm, request, jwtData);
        String refreshToken = jwtUtil.generateRefreshToken(algorithm, user.getUsername(), request);
        ResponseCookie.ResponseCookieBuilder jwtCookieBuilder = ResponseCookie.from(COOKIE_BEARER, jwtAccessToken);
        ResponseCookie.ResponseCookieBuilder refreshCookieBuilder = ResponseCookie.from(REFRESH_COOKIE, refreshToken);
        //if it work use spring profile
        if (!Objects.equals(applicationConfiguration.getAppEnvMode(), Environnement.PRODUCTION.toString().toLowerCase())) {
            jwtCookieBuilder.httpOnly(false);
            jwtCookieBuilder.secure(false);
            refreshCookieBuilder.httpOnly(false);
            refreshCookieBuilder.secure(false);
        } else {
            jwtCookieBuilder.httpOnly(true);
            jwtCookieBuilder.secure(true);
            refreshCookieBuilder.secure(true);
            refreshCookieBuilder.httpOnly(true);
        }

        ResponseCookie jwtCookie = jwtCookieBuilder.path("/")
                .sameSite("Lax")
                .maxAge(jwtUtil.EXPIRATION_TIME)
                .build();
        ResponseCookie refreshCookie = refreshCookieBuilder
                .maxAge(jwtUtil.REFRESH_EXPIRATION_TIME)
                .path(JwtUtil.REFRESH_TOKEN_ENDPOINT)
                .sameSite("Lax")
                .build();
        response.addHeader(HttpHeaders.SET_COOKIE, jwtCookie.toString());
        response.addHeader(HttpHeaders.SET_COOKIE, refreshCookie.toString());

        createJSONResponseContent(response, claims, HttpStatus.OK);

    }


    private static class UsernamePasswordRequest extends HashMap<String, String> {
        // Nothing, just a type marker
    }
}