package com.mouclepatrick.personalcoaching.controllers;

import com.mouclepatrick.personalcoaching.controllers.exception.ResourceNotFoundException;
import com.mouclepatrick.personalcoaching.dto.ModifierReservationDto;
import com.mouclepatrick.personalcoaching.dto.SeanceDto;
import com.mouclepatrick.personalcoaching.models.Reservation;
import com.mouclepatrick.personalcoaching.response.ApiResponse;
import com.mouclepatrick.personalcoaching.services.ClientService;
import com.mouclepatrick.personalcoaching.services.EmailService;
import com.mouclepatrick.personalcoaching.services.ReservationService;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/clients")
public class ClientController {
    private final ClientService clientService;
    private final ReservationService reservationService;
    private final EmailService emailService;
    @Value("${mail.from}")
    private String mailSender;

    @Autowired
    public ClientController(ClientService clientService, ReservationService reservationService, EmailService emailService) {
        this.clientService = clientService;
        this.reservationService = reservationService;
        this.emailService = emailService;
    }

    @GetMapping("/{id}/seances/valide")
    @PreAuthorize("hasAnyAuthority('client','admin')")
    public Map<String, Object> getSeances(@PathVariable("id") Integer clientId) {
        List<SeanceDto> upcomingSeances = clientService.getUpcomingReservations(clientId);
        List<SeanceDto> pendingSeances = clientService.getPendingSeances(clientId);
        List<SeanceDto> passedSeances = clientService.getPassedReservations(clientId);
        Map<String, Object> response = new HashMap<>();
        response.put("pendingSeances", pendingSeances);
        response.put("passedSeances", passedSeances);
        response.put("upcomingSeances", upcomingSeances);
        return response;

    }

    @PutMapping("/{userId}/reservations/{id}")
    @PreAuthorize("hasAnyAuthority('client','admin')")
    public ResponseEntity<ApiResponse> changeReservationDate(@Valid @RequestBody ModifierReservationDto modifierReservationDto) {
        Reservation reservation = reservationService.findBy(modifierReservationDto.getId());
        if (reservation == null) {
            throw new ResourceNotFoundException(modifierReservationDto.getId(), Reservation.class);
        }
        reservation.setDateSeance(modifierReservationDto.getNewDate().toLocalDate());
        reservation.setHeureSeance(modifierReservationDto.getNewDate().toLocalTime());
        reservationService.update(reservation);
        ApiResponse apiResponse = new ApiResponse(HttpStatus.OK, "La  réservation à bien été changée");
        return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
    }

    @PutMapping("/{clientId}/reservations/{reservationId}/canceled")
    @PreAuthorize("hasAnyAuthority('client','admin')")
    public ResponseEntity<ApiResponse> refuserSeance(@PathVariable Integer clientId, @PathVariable Integer reservationId) {
        boolean reservationExiste = clientService.reservationExiste(reservationId, clientId);
        ApiResponse apiResponse;
        if (!reservationExiste) {
            apiResponse = ApiResponse.builder().status(HttpStatus.BAD_REQUEST).message("this Seance doesn't exist for this coach").build();
            return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
        }
        boolean isReservationPending = clientService.isReservationPending(reservationId);

        if (!isReservationPending) {
            apiResponse = ApiResponse.builder().status(HttpStatus.BAD_REQUEST).message("This Seance is not  in a pending state ").build();
            return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
        }
        int hasRefused = clientService.canceledReservation(reservationId);
        Reservation reservation= reservationService.findBy(reservationId);
        if (hasRefused == 1) {
            Map<String, Object> templateModel = new HashMap<>();
            templateModel.put("recipientName", reservation.getCoach().getFullName());
            templateModel.put("clientName",reservation.getClient().getFullName());
            templateModel.put("programmeDate",reservation.getFullDateString());
            templateModel.put("activiteName",reservation.getActivite().getLibelle());
            templateModel.put("programmeName",reservation.getActivite().getProgramme().getNom());
            templateModel.put("activiteDuree", DurationFormatUtils.formatDurationWords(reservation.getActivite().getDuree().toMillis(),true,true));
            templateModel.put("senderName", mailSender);
            emailService.sendMessageUsingThymeleafTemplate(reservation.getCoach().getEmail(),
                    "Seance canceled","seance-canceled", templateModel);
            apiResponse = ApiResponse.builder()
                    .status(HttpStatus.OK)
                    .message("seance canceled successfully")
                    .build();
        } else {
            apiResponse = ApiResponse.builder()
                    .status(HttpStatus.BAD_REQUEST)
                    .message("unexepected error,can't canceled seance")
                    .build();
        }
        return new ResponseEntity<>(apiResponse, apiResponse.getStatus());

    }


}