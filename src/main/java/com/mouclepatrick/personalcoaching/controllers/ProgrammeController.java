package com.mouclepatrick.personalcoaching.controllers;

import com.mouclepatrick.personalcoaching.dto.ProgrammeDto;
import com.mouclepatrick.personalcoaching.models.Programme;
import com.mouclepatrick.personalcoaching.response.ApiResponse;
import com.mouclepatrick.personalcoaching.services.ProgrammeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class ProgrammeController {
    private final ProgrammeService programmeService;

    @Autowired
    public ProgrammeController(ProgrammeService programmeService) {
        this.programmeService = programmeService;
    }

    @GetMapping("/programmes/{id}")
    public ProgrammeDto programmes(@PathVariable int id) {

        return programmeService.getProgrammeDTO(id);
    }

    @PutMapping("/programmes/{id}")
    @PreAuthorize("hasAnyAuthority('admin','coach')")
    public ResponseEntity<ApiResponse> modifyProgramme(@Valid @RequestBody ProgrammeDto programmeDTO) {
        Programme newProgramme = programmeService.convertToEntity(programmeDTO);
        programmeService.updateProgramme(newProgramme);
        ApiResponse apiResponse = new ApiResponse(HttpStatus.OK, "Modification du programme effectuée");
        return new ResponseEntity<>(apiResponse, apiResponse.getStatus());

    }

    @PostMapping("/programmes")
    @PreAuthorize("hasAnyAuthority('admin','coach')")
    public ResponseEntity<ApiResponse> createProgramme(@Valid @RequestBody ProgrammeDto programmeDto) {
        Programme programme = programmeService.convertToEntity(programmeDto);
        programmeService.createProgramme(programme);
        ApiResponse apiResponse = new ApiResponse(HttpStatus.OK, "Creation du programme effectuée");
        return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
    }

}