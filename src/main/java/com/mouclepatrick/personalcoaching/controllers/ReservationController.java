package com.mouclepatrick.personalcoaching.controllers;

import com.mouclepatrick.personalcoaching.controllers.exception.ResourceNotFoundException;
import com.mouclepatrick.personalcoaching.dto.ReservationDTO;
import com.mouclepatrick.personalcoaching.dto.ReservationDetailsDto;
import com.mouclepatrick.personalcoaching.models.Activite;
import com.mouclepatrick.personalcoaching.models.Client;
import com.mouclepatrick.personalcoaching.models.Coach;
import com.mouclepatrick.personalcoaching.models.Programme;
import com.mouclepatrick.personalcoaching.models.Reservation;
import com.mouclepatrick.personalcoaching.models.UserPrincipal;
import com.mouclepatrick.personalcoaching.response.ApiResponse;
import com.mouclepatrick.personalcoaching.response.ApiResponseBody;
import com.mouclepatrick.personalcoaching.services.ClientService;
import com.mouclepatrick.personalcoaching.services.CoachService;
import com.mouclepatrick.personalcoaching.services.EmailService;
import com.mouclepatrick.personalcoaching.services.ProgrammeService;
import com.mouclepatrick.personalcoaching.services.ReservationService;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ReservationController {
    private final ProgrammeService programmeService;
    private final ClientService clientService;
    private final CoachService coachService;
    private final ReservationService reservationService;
    private final EmailService emailService;
    @Value("${mail.from}")
    private String mailSender;

    public ReservationController(ProgrammeService programmeService, ClientService clientService,
            CoachService coachService, ReservationService reservationService, EmailService emailService) {
        this.programmeService = programmeService;
        this.clientService = clientService;
        this.coachService = coachService;
        this.reservationService = reservationService;
        this.emailService = emailService;
    }

    @GetMapping(value = "/reservations/{id}")
    @PreAuthorize("hasAnyAuthority('client','admin','coach')")
    public ResponseEntity<ApiResponse> reservation(@PathVariable int id, Authentication authentication) {
        Reservation seance = reservationService.findBy(id);
        if (seance == null) {
            throw new ResourceNotFoundException(id, Reservation.class);
        }
        ApiResponse apiResponse;
        UserPrincipal currentUser = (UserPrincipal) authentication.getPrincipal();

        String currentUserRole = currentUser.getAuthorities().stream().findFirst().get().getAuthority();
        if ((!seance.getClient().getId().equals(currentUser.getId())
                && !seance.getCoach().getId().equals(currentUser.getId())) && !currentUserRole.equals("admin")) {
            apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, "it's not your reservation");
        } else {
            ReservationDetailsDto reservationResultDto = reservationService.toReservationResultDto(seance);
            apiResponse = new ApiResponseBody<>(HttpStatus.OK, reservationResultDto);
        }
        return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
    }

    @PostMapping(value = "/reservations")
    @PreAuthorize("hasAuthority('client')")
    public ResponseEntity<ApiResponse> reservation(@RequestBody ReservationDTO reservationDTO) {
        Client client = clientService.findBy(reservationDTO.getClientId());
        Coach coach = coachService.getCoach(reservationDTO.getCoachId());
        if (coach != null && client != null) {
            Programme programme = programmeService.getProgramme(reservationDTO.getProgrammeId());
            if (!programme.getCoach().equals(coach)) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "programme not owned by the coach");
            }
            Optional<Activite> activite = programme.getActivites().stream()
                    .filter(anActivite -> anActivite.getId() == reservationDTO.getActiviteId()).findFirst();
            if (activite.isPresent()) {
                boolean isReservationPossible = reservationService.isReservationPossible(reservationDTO.getCoachId(),
                        reservationDTO.getDate().toLocalDate(), reservationDTO.getDate().toLocalTime(),
                        reservationDTO.getDate().toLocalTime().plusSeconds(activite.get().getDuree().getSeconds()));
                if (isReservationPossible) {
                    Reservation reservation = new Reservation();
                    reservation.setActivite(activite.get());
                    reservation.setHeureSeance(reservationDTO.getDate().toLocalTime());
                    reservation.setEtatSeance(Reservation.Etat.PENDING.toString());
                    reservation.setDateSeance(reservationDTO.getDate().toLocalDate());
                    reservation.setDateReservation(LocalDateTime.now());
                    reservation.setCoach(coach);
                    reservation.setClient(client);
                    Reservation reservedSeance = reservationService.add(reservation);
                    // prepare les données pour l'envoie de l'email
                    Map<String, Object> templateModel = new HashMap<>();
                    templateModel.put("recipientName", coach.getFullName());
                    templateModel.put("clientName", client.getFullName());
                    templateModel.put("programmeDate", reservation.getFullDateString());
                    templateModel.put("activiteName", reservation.getActivite().getLibelle());
                    templateModel.put("programmeName", reservation.getActivite().getProgramme().getNom());
                    templateModel.put("activiteDuree", DurationFormatUtils
                            .formatDurationWords(reservation.getActivite().getDuree().toMillis(), true, true));
                    templateModel.put("senderName", mailSender);
                    // envoie de l'email
                    emailService.sendMessageUsingThymeleafTemplate(coach.getEmail(),
                            "new  Seance", "new-seance", templateModel);
                    HashMap<String, String> payload = new HashMap<>();
                    payload.put("id", reservedSeance.getId().toString());
                    payload.put("message", "reservation Successful");
                    ApiResponse apiResponse = new ApiResponseBody<>(HttpStatus.OK, payload);
                    return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
                }
            }

        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "wrong information provided can process data");

    }
}