package com.mouclepatrick.personalcoaching.controllers;

import com.mouclepatrick.personalcoaching.controllers.exception.ResourceNotFoundException;
import com.mouclepatrick.personalcoaching.models.Activite;
import com.mouclepatrick.personalcoaching.models.Meeting;
import com.mouclepatrick.personalcoaching.repositories.ActiviteRepository;
import com.mouclepatrick.personalcoaching.services.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ScheduleController {
    private final ScheduleService scheduleService;
    private final ActiviteRepository activiteRepository;

    @Autowired
    public ScheduleController(ScheduleService scheduleService, ActiviteRepository activiteRepository) {
        this.scheduleService = scheduleService;
        this.activiteRepository = activiteRepository;
    }

    @GetMapping("/schedules")
    public List<Meeting> getSchedule(@RequestParam(value = "from")
                                    @DateTimeFormat(pattern = "yyyy-MM-dd")
                                            LocalDate dateDebut,
                                     @RequestParam("to")
                                    @DateTimeFormat(pattern = "yyyy-MM-dd")
                                            LocalDate dateEnd, @RequestParam("id") int coachId
            , @RequestParam("activity_id") int activiteId) {
        var activite = activiteRepository.findById(activiteId).orElse(null);
        if(activite==null){
            throw new ResourceNotFoundException(activiteId, Activite.class);
        }
        if (activite.getProgramme().getCoach().getId()!=coachId) {
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST,"activite not owned by the coach");
        }
        var coach= activite.getProgramme().getCoach();
        return scheduleService.getMeetings(coach, dateDebut, dateEnd, activite.getDuree());
    }
}