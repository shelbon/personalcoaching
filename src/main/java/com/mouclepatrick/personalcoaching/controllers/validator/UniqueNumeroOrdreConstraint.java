package com.mouclepatrick.personalcoaching.controllers.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = UniqueNumeroOrdreValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueNumeroOrdreConstraint {
    String message() default "numero ordre ne peut pas être identique au autre activite";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}