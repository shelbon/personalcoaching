package com.mouclepatrick.personalcoaching.controllers.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator implements
        ConstraintValidator<ValidEmail, String> {
    final String REGEX = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
    private Pattern pattern;
    private Matcher matcher;

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {

        return email != null && (validateEmail(email));
    }

    private boolean validateEmail(String email) {
        pattern = Pattern.compile(REGEX, Pattern.CASE_INSENSITIVE);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
}