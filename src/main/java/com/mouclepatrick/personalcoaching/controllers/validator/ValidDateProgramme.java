package com.mouclepatrick.personalcoaching.controllers.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = ValidDateProgrammeValidator.class)
@Target( { ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidDateProgramme {
    String message() default "Date de debut ne peut pas être après la date de fin";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}