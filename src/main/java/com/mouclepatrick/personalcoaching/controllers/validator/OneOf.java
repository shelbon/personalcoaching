package com.mouclepatrick.personalcoaching.controllers.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = OneOfConstraintValidator.class)
@Target( { ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface OneOf {
    String message() default "unknown user type";
    String [] comparator();
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}