package com.mouclepatrick.personalcoaching.controllers.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

public class OneOfConstraintValidator implements   ConstraintValidator<OneOf, String> {
    String []valueList;
    @Override
    public void initialize(OneOf constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
        valueList=constraintAnnotation.comparator();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return Arrays.stream(valueList).anyMatch((val)->val.equalsIgnoreCase(value));
    }
}