package com.mouclepatrick.personalcoaching.controllers.validator;

import com.mouclepatrick.personalcoaching.dto.ActiviteDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UniqueNumeroOrdreValidator implements
        ConstraintValidator<UniqueNumeroOrdreConstraint, List<ActiviteDTO>> {
    @Override
    public void initialize(UniqueNumeroOrdreConstraint constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(List<ActiviteDTO> list, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        Set<Integer> set = new HashSet<>();
        List<Integer> indexDoublon = new ArrayList<>();
        for (int index = 0; index < list.size(); index++) {
            final boolean isAdded = set.add(list.get(index).getNumeroOrdre());
            if (!isAdded) {
                indexDoublon.add(index);
            }
        }
        if (!indexDoublon.isEmpty()) {
            indexDoublon.forEach(index ->
                context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
                        .addContainerElementNode("activites[" + index + "]", List.class, 0)
                        .inIterable().atIndex(index)
                        .addPropertyNode("numeroOrdre")
                        .addConstraintViolation());

            return false;
        }
        return true;
    }
}