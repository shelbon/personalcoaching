package com.mouclepatrick.personalcoaching.controllers.validator;

import com.mouclepatrick.personalcoaching.dto.ProgrammeDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class ValidDateProgrammeValidator implements ConstraintValidator<ValidDateProgramme, ProgrammeDto> {
    @Override
    public boolean isValid(ProgrammeDto programme, ConstraintValidatorContext context) {
        final LocalDate dateDebut=programme.getDateDebut();
        final LocalDate dateFin=programme.getDateFin();
        if(dateDebut==null || dateFin==null){
            return false;
        }
        boolean isBeforeOrEqualDateFin = dateDebut.isBefore(programme.getDateFin()) ||dateFin.isEqual(programme.getDateFin());
        if (!isBeforeOrEqualDateFin) {
            context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
                    .addPropertyNode("dateDebut")
                    .addConstraintViolation();
            return false;
        }
        return true;
    }
}