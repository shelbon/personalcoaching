package com.mouclepatrick.personalcoaching.controllers.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = PasswordConstraintValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidPassword {
    String message() default "Password not strong enough at least 1 lowercase letter,1 Uppercase letter,1 number and 1 special character ";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}