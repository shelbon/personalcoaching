package com.mouclepatrick.personalcoaching.abstraction;

import java.util.List;

public interface BaseService<T> {
    T add(T entity);

    T findBy(Integer id);



    List<T> findAll();

    T update(T entity);
}