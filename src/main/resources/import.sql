INSERT INTO sujet (libelle) VALUES ('IT');
INSERT INTO sujet (libelle) VALUES ('DB beginner');
INSERT INTO sujet (libelle) VALUES ('DB advanced');

INSERT INTO utilisateur(type, email, nom, password, prenom, profile_picture, tel) VALUES ('client', 'johndoe-coachme@trash-mail.com', 'john','{bcrypt}$2y$10$2UPb3W47t4.ve86lEiFRM.nqsPlBaXdq0Qi/BTKdRt2ss0ivEasOW', 'doe', null, '0678912345');
INSERT INTO utilisateur(type, email, nom, password, prenom, profile_picture, tel, horaire_debut, horaire_fin) VALUES ('coach', 'janedoe-coachme@trash-mail.com', 'doe','{bcrypt}$2y$10$uFsaBiYPjaq6r8o9xeLMwuoW9E1Gn1oBEDe6ObDcoNdJbTqTBPQYe', 'jane', null, '0675912345', '9:00','17:00');
INSERT INTO utilisateur(type, email, nom, password, prenom, profile_picture, tel) VALUES ('client', 'paullange@example.com', 'lange','{bcrypt}$2y$10$QFklip.6A3F0iv9gwsirXu1saIfs9pkwWUKPRPhDrksy9BN1rHfru', 'paul', null, '0674912345');
INSERT INTO utilisateur(type, email, nom, password, prenom, profile_picture, tel, horaire_debut, horaire_fin) VALUES ('coach', 'evilbob@gmail.com', 'evil', '{bcrypt}$2y$10$TbNq.rjr6x4Y4qARskfIFeo7hGS8FwlkqZsUfvUDFMxqlNVYrzcRu','bob', null, '0615912345', '8:00', '15:00');
insert into programme (date_annulation, date_debut, date_fin, nom, coach_id) VALUES (null, '2022-02-25', '2022-08-25', 'Les bases du sql', 2);
insert into activite (description, libelle, no_ordre, duree, programme_id) values (NULL, 'bases de données, SQL et types de données', 1, 1440000000000, 1);
insert into activite (description, libelle, no_ordre, duree, programme_id) values (NULL, 'Les fonctions SQL et Les fonctions d''agrégation', 2, 1440000000000, 1);
insert into activite (description, libelle, no_ordre, duree, programme_id) values (NULL, 'Les contraintes d intégrité', 3, 3600000000000, 1);
INSERT INTO reservation (date_reservation,appreciation, date_annulation, date_seance, heure_seance, description, etat_seance, client_id,coach_id, activite_id) VALUES (CURRENT_DATE,null, null, CURRENT_DATE, '00:00:00', '', 'PERFORMED', 1, 2, 1);
INSERT INTO reservation (date_reservation,appreciation, date_annulation, date_seance, heure_seance, description, etat_seance, client_id,coach_id, activite_id) VALUES (CURRENT_DATE,null, null, CURRENT_DATE, '14:20:00', '', 'ACCEPTED', 1, 2, 2);
INSERT INTO reservation (date_reservation,appreciation, date_annulation, date_seance, heure_seance, description, etat_seance, client_id,coach_id, activite_id) VALUES (CURRENT_DATE,null, null, CURRENT_DATE, '10:00:00', '', 'PENDING', 1, 2, 3);

insert into utilisateur_sujet (coach_id, sujet_id) values (2, 2);
insert into utilisateur_sujet (coach_id, sujet_id) values (2, 3);
INSERT INTO programme_sujet (programme_id, sujet_id) VALUES (1, 1);
INSERT INTO programme_sujet (programme_id, sujet_id) VALUES (1, 2);
INSERT INTO programme_sujet (programme_id, sujet_id) VALUES (1, 3);
-- jeu d'essaie 2

INSERT INTO sujet (libelle) VALUES ('Java beginner');
INSERT INTO sujet (libelle) VALUES ('Java advanced');

insert into programme (date_annulation, date_debut, date_fin, nom, coach_id) VALUES (null, '2022-02-25', '2022-02-25', 'Les bases du java ', 4);
insert into activite (description, libelle, no_ordre, duree, programme_id) values (NULL, 'Les primitives', 0, 3600000000000, 2);
INSERT INTO reservation (appreciation, date_annulation, date_seance, heure_seance, description, etat_seance, client_id,coach_id, activite_id) VALUES (null, null, CURRENT_DATE, '8:50:00', '', 'PENDING', 3, 4, 4);
insert into utilisateur_sujet (coach_id, sujet_id) values (4, 1);
insert into utilisateur_sujet (coach_id, sujet_id) values (4, 4);
INSERT INTO programme_sujet (programme_id, sujet_id) VALUES (2, 1);
INSERT INTO programme_sujet (programme_id, sujet_id) VALUES (2, 4);

-- jeu d'essaie 3

INSERT INTO sujet (libelle) VALUES ('HTML beginner');
INSERT INTO utilisateur(type, email, nom, password, prenom, profile_picture, tel) VALUES ('client', 'bob@singer.com', 'singer', '{bcrypt}$2y$10$HD0k28FaSH2QokdYU2WCW.lq0fMqLX9DiZoqHFYSwHHF765L2mnsm','bob', null, '0624912345');
INSERT INTO utilisateur(type, email, nom, password, prenom, profile_picture, tel, horaire_debut, horaire_fin) VALUES ('coach', 'sadjoy@gmail.com', 'sad', '{bcrypt}$2y$10$ZsltqRF4ckbYTKxzclwgD.ZBv2ldgQuZEJ4hOTq452qV0JvEfu0oW','joy', null, '0635912345', '8:00', '15:00');
INSERT INTO utilisateur(type, email, nom, password, prenom, profile_picture, tel) VALUES ('admin', 'bob@sinclar.com', 'sinclar', '{bcrypt}$2y$10$HD0k28FaSH2QokdYU2WCW.lq0fMqLX9DiZoqHFYSwHHF765L2mnsm','bob', null, '0614912345');
insert into programme (date_annulation, date_debut, date_fin, nom, coach_id) VALUES (null, '2022-02-06', '2022-02-06', 'Les bases du HTML ', 6);
insert into activite (description, libelle, no_ordre, duree, programme_id) values (NULL, 'Les language de balises', 0, 1440000000000, 3);
INSERT INTO reservation (appreciation, date_annulation, date_seance, heure_seance, description, etat_seance, client_id,coach_id, activite_id) VALUES (null, null, CURRENT_DATE, '8:00:00', '', 'PENDING', 5, 6, 5);


insert into utilisateur_sujet (coach_id, sujet_id) values (6, 1);
insert into utilisateur_sujet (coach_id, sujet_id) values (6, 6);
INSERT INTO programme_sujet (programme_id, sujet_id) VALUES (3, 1);
INSERT INTO programme_sujet (programme_id, sujet_id) VALUES (3, 6);