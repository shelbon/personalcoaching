/* eslint-env node */
require("@rushstack/eslint-patch/modern-module-resolution");

module.exports = {
  root: true,
  extends: [
    "plugin:vue/vue3-essential",
    "eslint:recommended",
    "@vue/eslint-config-typescript/recommended",
    "@vue/eslint-config-prettier",
  ],
  ignorePatterns: ["bootstrap.*.js"],
  env: {
    "vue/setup-compiler-macros": true,
    node: true,
  },
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
  },
  rules: {
    "vue/multi-word-component-names": [
      "error",
      {
        ignores: [
          "Meeting",
          "404",
          "About",
          "Coach",
          "Coachs",
          "Dashboard",
          "Forbidden",
          "Home",
          "Login",
          "Seances",
          "Profile",
          "Programmes",
          "Programme",
          "Settings",
          "Signup",
          "Users",
          "Sujets",
        ],
      },
    ],
  },
  overrides: [
    {
      files: ["cypress/**/**.{js,ts,jsx,tsx}"],
      extends: ["plugin:cypress/recommended"],
      rules: {
        "no-undef": "off",
      },
    },
  ],
};
