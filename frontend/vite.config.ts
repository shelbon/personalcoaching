import vue from "@vitejs/plugin-vue";
import ElementPlus from "unplugin-element-plus/vite";
import {fileURLToPath, URL} from "url";

import {defineConfig} from "vite";
import EnvironmentPlugin from "vite-plugin-environment";
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    EnvironmentPlugin({
      API_BASE_URL: "/api",
    }),
    ElementPlus(),
  ],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  optimizeDeps: {
    exclude: ["underscore"],
  },
  build: {
    outDir: "build/dist/public",
  },
});