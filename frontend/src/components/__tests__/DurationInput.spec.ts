import {describe, it, expect} from "vitest";

import {mount} from "@vue/test-utils";
import DurationInput from "../DurationInput.vue";

describe("DurationInput", () => {
    it("renders properly", () => {
        expect(DurationInput).toBeTruthy()
        const model = "24:00";
        const wrapper = mount(DurationInput, {props: {"v-model": model, 'error': false, 'is-valid': true}});
        expect(wrapper.html()).toContain("24:00");
    });
});