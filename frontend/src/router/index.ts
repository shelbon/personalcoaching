import {createRouter, createWebHistory} from "vue-router";
import MainNavigation from "../components/MainNavigation.vue";
import {useUserStore} from "../stores/user";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: "/",
            name: "home",
            meta: {
                title: "Home Page - Coach me",
                metaTags: [
                    {
                        name: "description",
                        content: "The home page of coach me app.",
                    },
                    {
                        property: "og:description",
                        content: "The home page of coach me app.",
                    },
                ],
            },
            components: {
                default: () => import("../views/Home.vue"),
                MainNavigation,
            },
        },
        {
            path: "/coachs",
            name: "coachs",
            components: {
                default: () => import("../views/Coachs.vue"),
                MainNavigation,
            },
        },
        {
            path: "/coach/:id",
            name: "coach",
            components: {
                default: () => import("../views/Coach.vue"),
                MainNavigation,
            },
        },
        {
            path: "/reservation/:id",
            name: "reservation result",
            props: true,
            components: {
                default: () => import("../views/ReservationResult.vue"),
                MainNavigation,
            }
        },
        {
            path: "/programme/:id",
            name: "programmes",
            components: {
                default: () => import("../views/Programme.vue"),
                MainNavigation,
            },
        },
        {
            path: "/profile",
            meta: {
                requiresAuth: true,
            },
            components: {
                default: () => import("../views/Profile.vue"),
                MainNavigation
            },
        },
        {
            path: "/profile/:id",
            meta: {
                requiresAuth: true,
            },
            component: () => import("../views/Profile.vue"),
        },
        {
            path: "/login",
            name: "login",
            component: () => import("../views/Login.vue"),
        },
        {
            path: "/signup",
            name: "signup",
            component: () => import("../views/Signup.vue"),
        },
        {
            path: "/admin",
            name: "admin home",
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from) => {
                // reject the navigation
                const userStore = useUserStore();
                if (
                    !userStore.isAuthenticated &&
                    userStore.getUser()?.role !== "admin"
                ) {
                    return "/forbidden";
                }
                return true;
            },
            components: {
                default: () => import("../views/Dashboard.vue"),
            },
            children: [
                {
                    path: "",
                    name: "admin",
                    components: {
                        default: () => import("../views/admin/Home.vue"),
                    },
                },
                {
                    path: "users",
                    name: "admin users",
                    components: {
                        default: () => import("../views/admin/Users.vue"),
                    },
                },
                {
                    path: "users/:id",
                    name: "user profile",
                    components: {
                        default: () => import("../views/Profile.vue"),
                    },
                },
                {
                    path: "users/:id/seances",
                    name: "user seances",
                    components: {
                        default: () => import("../views/client/Seances.vue"),
                    },
                },
                {
                    path: "coach/:id/seances",
                    name: "coach seances",
                    components: {
                        default: () => import("../views/coachs/Seances.vue"),
                    },
                },
                {
                    path: "coach/:id/programmes",
                    name: "coach programmes",
                    components: {
                        default: () => import("../views/Programmes.vue"),
                    },
                },
                {
                    path: "coach/:coachId/programmes/modify/:programmeId",
                    name: "coach modify programmes",
                    components: {
                        default: () => import("../views/ModifyProgramme.vue"),
                    },
                },
                {
                    path: "profile",
                    name: "admin profile",
                    components: {
                        default: () => import("../views/Profile.vue"),
                    },
                },
            ],
        },
        {
            path: "/client",
            name: "client dashboard",
            meta: {
                title: "client dasboard - Coach me",
                requiresAuth: true,
            },
            beforeEnter: (to, from) => {
                const userStore = useUserStore();
                if (
                    !userStore.isAuthenticated &&
                    userStore.getUser()?.role !== "client"
                ) {
                    return "/forbidden";
                }
                return true;
            },
            component: () => import("../views/Dashboard.vue"),
            children: [
                {
                    path: "",
                    name: "client seances",
                    component: () => import("../views/client/Seances.vue")
                },
                {
                    path: "profile",
                    name: "client profile",
                    component: () => import("../views/Profile.vue")
                }
            ]
        },
        {
            path: "/coach",
            name: "dashboard",
            meta: {
                title: "coach dasboard - Coach me",
                requiresAuth: true,
            },
            beforeEnter: (to, from) => {
                const userStore = useUserStore();
                if (
                    !userStore.isAuthenticated &&
                    userStore.getUser()?.role !== "coach"
                ) {
                    return "/forbidden";
                }
                return true;
            },
            component: () => import("../views/Dashboard.vue"),
            children: [
                {
                    path: "settings",
                    name: "dashboard settings",
                    component: () => import("../views/Settings.vue"),
                },
                {
                    path: "profile",
                    name: " dashboard profile",
                    component: () => import("../views/Profile.vue"),
                },
                {
                    path: "seances",
                    name: "coach seances",
                    component: () => import("../views/coachs/Seances.vue"),
                },
                {
                    path: "programmes",
                    name: "dashboard programmes",
                    component: () => import("../views/Programmes.vue"),
                },
                {
                    path: "programmes/modify/:id",
                    name: " dashboard modify programme",
                    component: () => import("../views/ModifyProgramme.vue"),
                },
                {
                    path: "programmes/create",
                    name: " dashboard create programme",
                    component: () => import("../views/ModifyProgramme.vue"),
                },
            ],
        },
        {
            path: "/forbidden",
            name: "access refuse",
            component: () => import("../views/Forbidden.vue"),
        },
        {
            path: "/:pathMatch(.*)*",
            name: "404 not found",
            component: () => import("../views/404.vue"),
        },
    ],
});
router.beforeEach((to, from) => {
    const userStore = useUserStore();
    if (to.meta.requiresAuth && !userStore.isAuthenticated) {
        return {
            path: "/login",
            // save the location we were at to come back later
            query: {redirect: to.fullPath},
        };
    }

    if (to.path.includes("reservation/") && userStore?.getUser()?.role.toLowerCase() !== "client" ||
        to.path.includes("programme/") && userStore.isAuthenticated
        && userStore?.getUser()?.role.toLowerCase() !== "client") {
        return "/forbidden";
    }
});

export default router;