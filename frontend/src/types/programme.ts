export interface Programme {
  dataAnulation: string | null;
  dateDebut: string;
  dateFin: string;
  id: number;
  nom: string;
}
