import type { Sujet } from "./sujet";
import type { Programme } from "./programme";
import type { Seance } from "./seance";

export interface Coach {
  id: number;
  nom: string;
  prenom: string;
  seances: Array<Seance>;
  sujets: Array<Sujet>;
  programmes: Array<Programme>;
  imageProfile: string;
}
