export interface Seance {
  appreciation: string | null;
  dateAnnulation: string | null;
  dateSeance: string | null;
  heureSeance: string | null;
  description: string;
  etatSeance: string;
  id: number;
}
