export default class Errors {
    /**
     * Creation d’une instance de Errors
     */
    constructor() {
        this.errors = [];
    }

    /**
     * Enregistre les erreurs
     *
     * @param {object} reponse
     */
    record(reponse) {
        if (reponse) {
            if (Object.hasOwn(reponse, "subErrors")) {
                this.errors = reponse.subErrors;
            }
        }

    }

    /**
     *Indique si on as des erreurs.
     */
    any() {
        return this.errors.length > 0;
    }

    add(error) {
        const indexError = this.errors.findIndex(
            (thisError) => thisError.field == error.field
        );
        if (indexError >= 0) {
            this.errors.splice(indexError, 1, error);
        } else {
            this.errors.push(error);
        }
    }

    /**
     * Indique si une erreur existe pour un champ
     *
     * @param {string} field
     */
    has(field) {
        let fieldError;
        if (Array.isArray(this?.errors)) {
            fieldError = this?.errors?.find((error) => error.field === field);
        }
        return fieldError || false;
    }
    /**
     * Récupère le message d’une erreur
     *
     * @param {string} field
     */
    get(field) {
        const erreur = this.has(field);
        if (erreur) {
            return erreur.message;
        }
    }

    /**
     * Efface une  erreur
     *
     * @param {string|null} field
     */
    clear(field) {
        if (field) {
            for (let i = 0; i < this.errors.length; i++) {
                if(this.errors[i]?.field.includes(field)){
                    this.errors.splice(i, 1);
                }
                if (this.errors[i]?.field === field) {
                    this.errors.splice(i, 1);
                }
            }
        }
    }

    clearAll() {
        this.errors = [];
    }
}