import * as yup from "yup";
import isStrongPassword from "validator/es/lib/isStrongPassword";
import dayjs from "dayjs";
import {get} from "underscore/underscore-esm";

yup.addMethod(yup.string, "password", function (message) {
    return this.test("isStrongPassword", function (password) {
        const isPasswordValid = isStrongPassword(password);
        if (!isPasswordValid) {
            return this.createError({path: this.path, message});
        }
        return true;
    });
});
yup.addMethod(yup.string, "different", function (field, message) {
    return this.test("different", function (currentField) {
        const comparedField = this.parent[field];
        const different = currentField !== comparedField;
        if (!different) {
            return this.createError({path: this.path, message})
        }
        return true;
    });
});
yup.addMethod(yup.string, "sameAs", function (field, message) {
    return this.test("sameAs", function (currentField) {
        const comparedField = this.parent[field];
        const isSame = currentField === comparedField;
        if (!isSame) {
            return this.createError({path: this.path, message})
        }
        return true;
    });
});

yup.addMethod(yup.date, "beforeOrEqual", function (otherFieldName, message) {
    return this.test("before", function (testedField) {
        const testedDate = dayjs(testedField);
        const otherDate = dayjs(this.parent[otherFieldName]);
        if (!otherDate.isValid()) {
            return true;
        }
        const isValid = testedDate <= otherDate;
        if (!isValid) {
            return this.createError({path: this.path, message});
        }
        return true;
    });
});
yup.addMethod(
    yup.date,
    "afterOrEqual",
    function (compared, message) {
        return this.test("afterOrEqual", function (testedField) {
            const testedDate = dayjs(testedField);
            let otherDate;
            if (typeof compared === "object") {
                otherDate = dayjs(compared);
            } else {
                otherDate = dayjs(this.parent[compared]);
            }
            if (!otherDate.isValid()) {
                return true;
            }
            const isValid = testedDate >= otherDate;
            if (!isValid) {
                return this.createError({path: this.path, message});
            }
            return true;
        });
    }
);


const uniquePropertyTest = function (value, propertyName, message) {
    if (this.parent.length > 1 &&
        this.parent
            .filter(v => v !== value)
            .some(v => get(v, propertyName).toString().toLowerCase() === get(value, propertyName).toString().toLowerCase())
    ) {
        return this.createError({
            path: `${this.path}.${propertyName}`,
            message
        });
    }

    return true;
};

yup.addMethod(yup.object, "uniqueProperty", function (propertyName, message) {
    return this.test('unique', message, function (value) {
        return uniquePropertyTest.call(this, value, propertyName, message);
    });
});