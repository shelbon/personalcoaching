const allowFileType = new Map();
allowFileType.set("image/jpeg", "jpeg");
allowFileType.set("image/png", "png");
allowFileType.set("image/svg+xml", "svg");
allowFileType.set("image/webp", "webp");
allowFileType.set("image/tiff", "tiff");
const fileMaxSizeInBytes = 5000000;
async function loadImage(imageBlob) {
    if (!imageBlob || !(imageBlob instanceof Blob)) {
        throw Error("image can't be null or is not a blob");
    }
    const fileReader = new FileReader();
    return new Promise((resolve, reject) => {

        fileReader.addEventListener("load", function () {
            resolve(fileReader.result);
        });
        fileReader.readAsDataURL(imageBlob);
    });
}
export {allowFileType, fileMaxSizeInBytes,loadImage};