import axiosApi from "axios";

const axios = axiosApi.create({
  withCredentials: true,
});
export default axios;