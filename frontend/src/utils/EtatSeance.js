export const
    EtatSeance = {
        PENDING: "PENDING",
        ACCEPTED: "ACCEPTED",
        PERFORMED: "PERFORMED",
        CANCELED: "CANCELED",
        REFUSED: "REFUSED"
    }