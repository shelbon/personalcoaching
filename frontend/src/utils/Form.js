import Errors from "./Errors";
import axios from "./axios";

export default class Form {
    /**
     * Création d’une nouvelle instance de Form
     *
     * @param {object} data initial data
     * @param  {object } validationSchema validation schema
     */
    constructor(data, validationSchema) {
        this.originalData = data;
        for (let field in data) {
            this[field] = data[field];
        }
        this.validationSchema = validationSchema;
        this.errors = new Errors();
        this.isSubmitting = false;
        this.shouldResetOnSucess = true;
        this.status = [];
    }

    set resetOnSucess(value) {
        this.shouldResetOnSucess = value;
    }

    setStatus(statusField, status) {
        this.status = [...this.status, {type: statusField, message: status}];
    }


    getStatus() {
        return this.status;
    }

    validateField(field) {

        this.validationSchema
            .validateAt(field, this.data(), {abortEarly: false})
            .then(() => {
                this.errors.clear(field)
            })
            .catch(err => {
                if (err?.inner && err.inner.length > 0) {
                    err.inner.forEach(error => {
                        this.errors.add({field: error.path, message: error.message});
                    });
                } else if (err?.path) {
                    this.errors.add({field: err.path, message: err.message});
                }

            });
    }

    hasError(field) {
        return this.errors.has(field);
    }

    getError(field) {
        return this.errors.get(field);
    }

    async validate() {
        await this.validationSchema
            .validate(this.data(), {abortEarly: false})
            .catch(err => {
                if (err?.inner && err.inner.length > 0) {
                    err.inner.forEach(error => {
                        this.errors.add({field: error.path, message: error.message});
                    });
                }
            });

        return !this.errors.any();
    }

    /**
     * Envoyer le formulaire.
     *
     * @param {string} requestType
     * @param {string} url
     * @param {object} opts
     */
    submit(requestType, url, opts = {}) {
        this.isSubmitting = true;
        let data;
        if (opts?.headers?.["Content-Type"] === "multipart/form-data") {
            data = this.toFormData(this.data());
        } else {
            data = this.data();
        }
        return new Promise((resolve, reject) => {
            axios[requestType](url, data, opts)
                .then((response) => {
                    this.isSubmitting = false;
                    this.onSuccess(response.data);
                    resolve(response.data);
                })
                .catch((error) => {
                    this.isSubmitting = false;
                    this.onFail(error?.response.data);
                    reject(error?.response.data);
                });
        });
    }

    clearStatus() {
        this.status = [];
    }

    toFormData(data) {
        let formData = new FormData();
        let inputData = null;
        for (const input in data) {
            inputData = data[input];
            if (inputData == null) {
                formData.append(input, "");
            } else if (Array.isArray(inputData)) {
                inputData.forEach((data,index)=>{
                    for (const property in data) {
                        formData.append(`${input}[${index}].${property}`, data[property]);
                    }

                })

            } else {
                formData.append(input, inputData);
            }
        }
        return formData;
    }

    /**
     *Ce qui se passe si le formulaire  envoyer est valide.
     *
     * @param _data
     */
    onSuccess(_data) {
        if (this.shouldResetOnSucess) {
            this.reset();
        }
    }

    /**
     * Si l’envoi du formulaire  echoue ou que le formulaire n’est pas valide
     *
     * @param {object} errors
     */
    onFail(errors) {
        this.errors.record(errors);
    }

    /**
     * Envoi une requête POST a un URL
     * .
     * @param {string} url
     * @param {object} opts
     */
    post(url, opts = {}) {
        return this.submit("post", url, opts);
    }

    /**
     * Envoi une requête PUT a un URL
     * .
     * @param {string} url
     * @param opts
     */
    put(url, opts = {}) {
        return this.submit("put", url, opts);
    }

    /**
     * Envoi une requête PATCH a un URL
     * .
     * @param {string} url
     * @param opts
     */
    patch(url, opts = {}) {
        return this.submit("patch", url, opts);
    }

    /**
     * Envoi une requête post a un URL
     * .
     * @param {string} url
     * @param opts
     */
    delete(url, opts = {}) {
        return this.submit("delete", url, opts);
    }

    /**
     * Vide les champs
     */
    reset() {
        for (let field in this.originalData) {
            this[field] = "";
        }

        this.errors.clearAll();
    }

    /**
     * Recupere les données du formulaire
     */
    data() {
        let data = {};

        for (let property in this.originalData) {
            data[property] = this[property];
        }

        return data;
    }
}