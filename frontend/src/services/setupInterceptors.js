import Cookies from "js-cookie";
import axios from "../utils/axios";
import {useUserStore} from "../stores/user";
import EventBus from "../common/EventBus";
import {ElMessage} from "element-plus";
import {v4 as uuidv4} from "uuid";

const url = `${process.env.API_BASE_URL}/auth/refresh-token`;
const setup = () => {
    axios.interceptors.request.use(
        (config) => {
            if (
                config.url === `${process.env.API_BASE_URL}/auth/login` ||
                config.method.toLocaleLowerCase() === "post" ||
                config.method.toLocaleLowerCase() === "put" ||
                config.method.toLocaleLowerCase() === "delete"
            ) {
                console.log("intercept request");
                const csrfToken = uuidv4();
                console.log(process.env.NODE_ENV);
                Cookies.set("XSRF-TOKEN", csrfToken, {
                    expires: new Date(new Date().getTime() + 60 * 1000),
                    secure: process.env.NODE_ENV === "production",
                    sameSite: "lax",
                });
                config.headers["X-XSRF-TOKEN"] = csrfToken;
            }
            return config;
        },
        (error) => {
            return Promise.reject(error);
        }
    );
    axios.interceptors.response.use(
        (res) => {
            return res;
        },
        async (err) => {
            const userStore = useUserStore();
            const originalConfig = err.config;

            if (
                originalConfig?.url !== `${process.env.API_BASE_URL}/auth/login` &&
                originalConfig.url !=
                `${process.env.API_BASE_URL}/auth/refresh-token` &&
                err.response
            ) {
                // Access Token has expired
                if (err.response.status === 401 && !originalConfig._retry) {
                    originalConfig._retry = true;
                    try {
                        const rs = await axios.get(url);
                        userStore.refreshToken(rs.data);
                        return axios(originalConfig);
                    } catch (err) {
                        // both access token and refresh token has expired
                        if (
                            err.response?.status === 401 &&
                            err?.response?.data?.status === "UNAUTHORIZED"
                        ) {
                            EventBus.dispatch("logout");
                            ElMessage({
                                message: err.response.data.message,
                                grouping: true,
                                type: "error",
                            });
                        }
                        return Promise.reject(err);
                    }
                }
            }

            return Promise.reject(err);
        }
    );
};
export default setup;
