import { library } from "@fortawesome/fontawesome-svg-core";
import { faCircleXmark, far } from "@fortawesome/free-regular-svg-icons";
import {
  faChevronDown,
  faChevronLeft,
  faChevronRight,
  faChevronUp,
  fas,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { BrowserTracing } from "@sentry/tracing";
import * as Sentry from "@sentry/vue";
import { createPinia } from "pinia";
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";
import { ElLoading } from "element-plus";
import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import setupInterceptors from "./services/setupInterceptors";
import "./utils/yupMethod.js";

library.add(
  faChevronRight,
  faChevronLeft,
  faChevronDown,
  faChevronUp,
  fas,
  far,
  faCircleXmark
);
const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);

const app = createApp(App);
app.use(router);
app.use(pinia);
app.use(ElLoading);
app.component("FontAwesomeIcon", FontAwesomeIcon);
setupInterceptors();
Sentry.init({
  app,
  logErrors: true,
  dsn: "https://570e224b86d84f6ab1bb0d06287c3315@o1173475.ingest.sentry.io/6268613",
  integrations: [
    new BrowserTracing({
      routingInstrumentation: Sentry.vueRouterInstrumentation(router),
      tracePropagationTargets: ["localhost", /^\//],
    }),
  ],

  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 0.7,
});

app.mount("#app");
