import {defineStore} from 'pinia';
import jwt_decode from "jwt-decode";

export const useUserStore = defineStore('user', {
    state: () => {
        return {isAuthenticated: false, user: null};
    },
    actions: {
        setIsAuthenticated(value) {
            if (value || value === false) {
                this.isAuthenticated = value;
            }
        },
        setUser(user) {
            if (user) {
                this.user = user;
            }

        },
        getUser() {
            return this.user;
        },
        clear() {
            this.$reset();
        },
        refreshToken(user) {
            this.isAuthenticated = true;
            this.user=user;
        }
    },
    persist: {
        enabled: true,
    },
});