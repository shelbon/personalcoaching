import {defineStore} from 'pinia';
import router from '../router';
import axios from '../utils/axios';
import {useUserStore} from './user';

const logoutUrl = `${process.env.API_BASE_URL}/auth/logout`;
const loginUrl = `${process.env.API_BASE_URL}/auth/login`;
const signUpUrl = `${process.env.API_BASE_URL}/auth/signup`;
export const useAuthStore = defineStore('auth', {
    state: () => {
        return {};
    },
    actions: {
        logout(redirectPath = {name: 'home'}) {
            const userStore = useUserStore();
            userStore.clear();
            axios.post(logoutUrl);
            router.push(redirectPath);
        },
        loginOrSignup(credentials, callbackSuccess, callbackError, type = 'login') {
            const userStore = useUserStore();
            const currentRoute=router?.currentRoute._value;
            axios
                .post(type === 'login' ? loginUrl : signUpUrl, credentials)
                .then((response) => {
                    userStore.setIsAuthenticated(true);
                    userStore.setUser(response.data.payload ?? response.data);
                    callbackSuccess();
                    if(currentRoute?.query?.redirect){
                        setTimeout(() => router.push(currentRoute.query.redirect), 2500);
                        return;
                    }
                    if (userStore.getUser()?.role === 'coach') {
                        setTimeout(() => router.push({name: 'dashboard'}), 2500);
                    } else if (userStore.getUser()?.role === 'client') {
                        setTimeout(() => router.push({name: 'home'}), 2500);
                    } else if (userStore.getUser()?.role === 'admin') {
                        setTimeout(() => router.push({name: 'admin home'}), 2500);
                    }
                })
                .catch((err) => callbackError(err));
        },
    },
});