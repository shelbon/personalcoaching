/// <reference types="cypress" />
// https://docs.cypress.io/api/introduction/api.html

describe("annonymous user, book a a meeting with a coach", () => {
  it("can access the  home page", () => {
    //visite la page d'accueil
    cy.visit("/");
    // click sur le button "voir coachs"
    cy.get("[data-cy=cta-home]").click();
  });
  it("can see coaches", () => {
    /*
          Regarde  si la liste des coachs à au moins un element et
          clique sur le button allant vers la page de detail du coach
        *
        * */
    cy.get("[data-cy=coaches]")
      .should("have.lengthOf.at.least", 1)
      .children()
      .get("[data-cy=cta-coach-item]")
      .each((element) => element.children("a") !== null);
  });
  it("go to the  first coach profile", () => {
    cy.get("[data-cy=coaches]")
      .should("have.lengthOf.at.least", 1)
      .children()
      .get("[data-cy=cta-coach-item]")
      .first()
      .click();
  });
  it("the first coach has a programme", () => {
    /*
         Regarde si le coach à au moins un programme et
         clique sur le button allant vers la page de réservation du programme
       *
       * */
    cy.get("[data-cy=programmes]")
      .should("have.lengthOf.at.least", 1)
      .children()
      .get("[data-cy=programme_cta]")
      .first()
      .click();
  });
  it("book the first programme", () => {
    // reserve une séance
    cy.bookFirstProgram();
    /*  vérifie que le modal de demande de connexion
          s'affiche et clique sur "se connecter"

        *  */
    cy.get("[data-cy=need-login-dialog]")
      .should("be.visible")
      .get("[data-cy=dialog-button-login]")
      .click();
    cy.intercept("GET", "/api/programmes/**").as("getProgrammes");
    // ce connecte a son profil utilisateur
    cy.get("[data-cy=email]").type("johndoe-coachme@trash-mail.com");
    cy.get("[data-cy=password]").type("John123**");
    cy.get("[data-cy=submit-button]").click({ timeout: 5000 });
    cy.url().should("not.contain", "/login?redirect");
    //ajoute un intercepteur sur cette url

    // attend qu'il récupère une nouvelle fois les programmes
    cy.wait("@getProgrammes");
   // vérifie que les cookies de connexion sont présents
    cy.getCookies({ timeout: 5000 }).then((cookies) => {
      const neededCookiesNames =
        cookies
          ?.filter(
            (cookie) =>
              cookie.name === "XSRF-TOKEN" || cookie.name === "REFRESH-COOKIE" || cookie.name==="COOKIE-BEARER"
          )
          ?.map((cookie) => cookie.name) ?? [];
      expect(neededCookiesNames).contain("XSRF-TOKEN");
      expect(neededCookiesNames).contain("REFRESH-COOKIE");
      expect(neededCookiesNames).contain("COOKIE-BEARER");
    });
    // reserve une séance
    cy.bookFirstProgram();
  });
  it("should have successfully booked", () => {
    // attend qu'il récupère le résultat de la réservation
    cy.intercept("GET","/api/reservations/**").as("getReservationResult");
    cy.wait("@getReservationResult");
    // verifie que la page de récap affiche
    // bien un message de réussite
    cy.get("[data-cy=booking-detail]")
      .should("be.visible")
      .children()
      .get(".icon-success")
      .should("be.visible");
  });
});