// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
Cypress.Commands.add("bookFirstProgram", () => {
  /*
   choisi une activité d'un programme et la date de la séance
  *et valide sont choix
  *
  * */
  cy.intercept("GET","/api/schedules**").as('getPlanning')
  cy.get("[data-cy=activity-radio-button]").first().click();
  cy.get("[data-cy=toggle-planning]").click();
  cy.wait('@getPlanning');
  cy.get("[data-cy=meeting-planner]")
    .should("be.visible")
    .children()
    .get("[data-cy=meeting-item")
    .first()
    .click();

  cy.get("[data-cy=booking-button]").should("not.be.disabled").click();
});