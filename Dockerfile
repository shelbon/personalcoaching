# App Building phase --------
FROM  gradle:8.4-jdk11 AS build
ARG APP_MODE

COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle  clean bootjar --no-daemon
RUN cp /home/gradle/src/build/libs/*.jar /home/gradle/src/coach-me.jar
# End App Building phase --------

# Container setup --------
FROM eclipse-temurin:11-jre-alpine

ENV APP_MODE=$APP_MODE
WORKDIR ./
EXPOSE 8080
RUN mkdir /app
COPY --from=build ["/home/gradle/src/coach-me.jar" ,"/app"]
RUN addgroup --system juser
RUN adduser -S -s /bin/false -G juser juser
RUN chown -R juser:juser /app
USER juser
ENTRYPOINT ["java","-jar","/app/coach-me.jar"]

# End Container setup --------
